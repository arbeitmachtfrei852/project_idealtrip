import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("comments").del();
    await knex("individual_materials").del();
    await knex("group_materials").del();
    await knex("journeys").del();
    await knex("users_trips").del();
    await knex("trips").del();
    await knex("users").del();
    
    // Inserts seed entries
    const userIds = await knex("users").insert([{
        email: "test@gmail.com",
        password: await hashPassword("1234")
    }, {
        email: "jose@gmail.com",
        password: await hashPassword("1234")
    }, {
        email: "ethan@gmail.com",
        password: await hashPassword("1234")
    }
    ]).returning('id');


    const tripIds = await knex("trips").insert([
        { trip_name: "test_hongkong", start_date: "2021-03-20", end_date: "2021-03-25" },
        { trip_name: "test_japan", start_date: "2021-08-16", end_date: "2021-08-19" }
    ]).returning('id' );


    await knex("users_trips").insert([
        {owner_id: userIds[0], trip_id: tripIds[0]},
        {owner_id: userIds[0], trip_id: tripIds[1]},
        {owner_id: userIds[1], trip_id: tripIds[1]},
        {owner_id: userIds[2], trip_id: tripIds[1]},
    ])


    await knex("journeys").insert([
        {
            day: "2021-03-21",
            location_name: "迪士尼樂園",
            address: "Unnamed Road, Hong Kong Disneyland Resort, 香港",
            lat: 22.312771, 
            lng: 114.041931,
            note: "ethan未比錢",
            trip_id: tripIds[0],
            start_time: "14:55:42"
        },
        {
            day: "2021-03-21",
            location_name: "Tecky",
            address: "Tecky1234",
            lat: 22.3908144, 
            lng: 113.9637543,
            note: "ethan未交學費",
            trip_id: tripIds[0],
            start_time: "15:52:42"
        },
        {
            day: "2021-03-22",
            location_name: "海洋公園",
            address: "香港香港島南區黃竹坑黃竹坑道180號",
            lat: 22.2466457, 
            lng: 114.1056825,
            note: "wilson未比錢",
            trip_id: tripIds[0],
            start_time: "12:55:42"
        }
    ])


    await knex("group_materials").insert([
        {
            item: "駕駛照",
            checked: false,
            assignee: "ethan",
            trip_id: tripIds[0],
        },
        {
            item: "腦",
            checked: false,
            assignee: "ethan",
            trip_id: tripIds[0],
        }
    ])
    

    await knex("individual_materials").insert([
        {
            item: "錢",
            checked: false,
            user_id: userIds[0],
            trip_id: tripIds[0],
        },
    ])

    await knex("comments").insert([
        {
            text: "hello",
            user_id: userIds[0],
            trip_id: tripIds[0],
        }
    ])
};
