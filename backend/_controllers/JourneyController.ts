import express from 'express';
import { JourneyService } from '../_services/JourneyService';
import moment from 'moment';

export class JourneyController {
    public constructor(private journeyService: JourneyService) { }

    public getExactlyTripData = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.journeyService.getExactlyTrip(req.params.id, req.user?.id);
            return res.json(result)
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "getExactlyTripData Unknown Error" })
        }
    }

    public delJourneyData = async (req:express.Request, res:express.Response)=>{
        try{
             await this.journeyService.delJourney(req.params.id)
            console.log(req.params.id)
            return res.json({success: true})

        }catch(e){
            console.error(e);
            return res.status(500).json({error:"delJourneyData Error"})
        }
    }

    public getJourneyData = async (req: express.Request, res: express.Response) => {
        try {
            const tripId = req.params.id

            const journeyData = await this.journeyService.getJourney(tripId);
            const tripData = await this.journeyService.getExactlyTrip(tripId, req.user?.id);

            let result: any = []

            for (let trip of tripData) {

                let start_date = new Date(moment(trip.start_date).format('YYYY-MM-DD'))
                let end_date = new Date(moment(trip.end_date).format('YYYY-MM-DD'))
                const days = Math.floor(
                    (Date.UTC(end_date.getFullYear(), end_date.getMonth(), end_date.getDate()) -
                        Date.UTC(start_date.getFullYear(), start_date.getMonth(), start_date.getDate()))
                    / (1000 * 60 * 60 * 24) + 1);


                for (let i = 0; i < days; i++) {
                    let start = start_date.setDate(start_date.getDate())
                    let day = moment(start).format('YYYY-MM-DD')

                    start = start_date.setDate(start_date.getDate() + 1)

                    result.push({ allDate: day, journey: [] })

                    for (let journey of journeyData) {
                        if (result[i].allDate == moment(journey.day).format('YYYY-MM-DD')) {
                            result[i].journey.push(journey)
                        }
                    }
                }
            }
            return res.json(result)
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "getJourneyData Unknown Error" })
        }
    }


    public getExactlyJourneyData = async (req:express.Request, res:express.Response)=>{
        try{
            const tripId = req.params.tripId
            const journeyItemId = req.params.journeyItemId

            const exactlyJourneyData = await this.journeyService.getExactlyJourney(tripId, journeyItemId)
            console.log(exactlyJourneyData)
            return res.json(exactlyJourneyData)
        }catch(e){
            console.error(e);
            return res.status(500).json({ error: "getExactlyJourneyData Unknown Error" })
        }
    }
}