import { UserService } from "../_services/UserService";
import express from 'express'


export class UserController {
    //@ts-ignore
    public constructor(private userService: UserService) {}



    public getCurrentUser = (req: express.Request, res: express.Response)=>{
        res.json(req.user);
    }

}