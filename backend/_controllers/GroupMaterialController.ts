import express from "express";
import { GroupMaterialService } from '../_services/GroupMaterialService';


export class GroupMaterialController {
    public constructor(private groupMaterialService: GroupMaterialService) { }

    public getMaterials = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.groupMaterialService.getMaterials(req.params.id);

            return res.status(200).json(result)

        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "Materials error" })
        }
    }

    public addMaterials = async (req: express.Request, res: express.Response) => {
        try {
            const { item, assignee } = req.body

            if (!item || !assignee) {
                return res.status(400).json({ error: "項目及負責人不能空白" })
            }

            const id = await this.groupMaterialService.addMaterials(
                req.params.id,
                item,
                assignee,
                false)


            return res.json({
                id: id,
                item: item,
                assignee: assignee,
                trip_id: req.params.id,
                checked: false,
            })
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "AddMaterial Error" })
        }
    }

    public delMaterials = async (req:express.Request, res:express.Response)=>{
        try{
            await this.groupMaterialService.delMaterials(req.params.id)
            return res.json({success:true})
        }catch(e){
            console.error(e);
            return res.status(500).json({error:"DelMaterial error"})
        }
    }





}