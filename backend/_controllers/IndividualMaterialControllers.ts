import {IndividualMaterialService} from "../_services/IndividualMaterialService";
import express from "express";

export class IndividualMaterialController {
    public constructor(private individualMaterialService: IndividualMaterialService){}

    public getMaterials = async (req:express.Request, res:express.Response)=>{
        try{
            const result = await this.individualMaterialService.getMaterials(req.params.id, req.user?.id);
            return res.status(200).json(result)
        } catch(e){
            console.error(e);
            return res.status(500).json({error:"IndMaterials Error"})
        }
    }

    public addMaterials = async (req:express.Request, res:express.Response)=>{
        try{
            const {item} = req.body 

            if (!item) {
                return res.status(400).json({ error: "項目不能空白" })
            }

            const id = await this.individualMaterialService.addMaterials(
                req.params.id,
                req.user?.id,
                item,
                false)


            return res.status(200).json({
                id: id,
                item: item,
                trip_id: req.params.id,
                user_id: req.user?.id,
                checked: false
            })
        }catch(e){
            console.error(e);
            return res.status(500).json({error:"IndMaterials Error"})
        }
    }

    public checkedMaterials = async (req:express.Request, res:express.Response)=>{
        try{
            const item_id = req.params.id
            const {checked} = req.body
            
            const result = await this.individualMaterialService.checkedMaterials(item_id,checked)
            return res.status(200).json({result, checked:!checked, item_id})

            
        }catch(e){
            console.error(e);
            return res.status(500).json({error: "CheckedMaterials error"})
        }
    }

    public deleteMaterials = async (req:express.Request, res:express.Response)=>{
        try{
            await this.individualMaterialService.delMaterials(req.params.id)
            return res.json({success:true})
        } catch(e){
            console.error(e);
            return res.status(500).json({error: "DeleteMaterials  Error"})
        }
    }

}

