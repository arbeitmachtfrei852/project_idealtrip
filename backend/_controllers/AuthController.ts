import { UserService } from "../_services/UserService";
import express, { Request, Response } from "express";
import { checkPassword, hashPassword } from "../hash";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";
import fetch from 'node-fetch';

export class AuthController {
    public constructor(private userService: UserService) { }

    public register = async (req: Request, res: Response) => {
        try {
            const { email, password, confirmedpassword } = req.body;

            if (!email || !password || !confirmedpassword) {
                return res.status(400).json({ error: "請填寫註冊資料" })
            }

            const userEmail = await this.userService.registerMatchEmail(email)
            for (let user of userEmail) {
                if (user.email == email) {
                    return res.status(401).json({ error: "電郵已註冊" })
                }
            }

            if (password !== confirmedpassword) {
                return res.status(401).json({ error: "密確不正確" })
            }


            const result = await this.userService.createUser(email, await hashPassword(password))


            return res.json(result)

        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "register error" });
        }
    }

    public login = async (req: express.Request, res: express.Response) => {
        try {
            const { email, password } = req.body;

            if (!email || !password) {
                return res.status(400).json({ error: "請輸入電郵及密碼" });
            }
            const user = await this.userService.getUserByEmail(email);


            if (user == null) {
                return res.status(401).json({ error: "用戶不存在" });
            }

            if (!await checkPassword(password, user.password)) {
                return res.status(401).json({ error: "電郵或密碼不正確" });
            }



            const payload = {
                id: user.id,
                email: user.email
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            const userInfo = res.json({
                token: token,
                user: user
            });

            return userInfo
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "Unknown Error" });
        }
    };

    public googleLogin = async (req: express.Request, res: express.Response) => {
        try {
            const accessToken = req.body.accessToken

            if (!accessToken) {
                return res.status(401).json({ error: "No Token" })
            }

            const googleRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                method: "get",
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            const json = await googleRes.json()
            // console.log(json)

            if (!json.id) {
                return res.status(400).json({ error: "No Token" })
            }

            let user = await this.userService.getUserByGoogleId(json.id)


            if (!user) {
                user = await this.userService.getUserByEmail(json.email);

                if (!user) {
                    user = await this.userService.createGoogleUser(json.email, json.id, await hashPassword('1234'))
                    user = await this.userService.getUserById(user.id)
                }
            }

            const payload = {
                id: user.id,
                email: user.email
            }

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            const userInfo = res.json({
                token: token,
                user: user
            });

            return userInfo

        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "GoogleLogin Unknown Error" })
        }
    }

    public loginFacebook = async (req: express.Request, res: express.Response) => {
        try {

            if (!req.body.accessToken) {
                return res.status(401).json({ msg: "FaceBook Wrong Access Token" })
            }

            const facebookRes = await fetch(`https://graph.facebook.com/me?access_token=${req.body.accessToken}&fields=id,name,email,picture`)
            const json = await facebookRes.json();

            if (!json.id) {
                return res.status(400).json({ error: "No token" })
            }
            let user = await this.userService.getUserByFacebookId(json.id);

            if (!user) {
                user = await this.userService.getUserByEmail(json.email)

                if (!user) {
                    user = await this.userService.createFacebookUser(json.email, json.id, await hashPassword('1234'))
                    user = await this.userService.getUserById(user.id)
                }
            }
            const payload = {
                id: user.id,
                email: user.email
            }

            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            const userInfo = res.json({
                token: token,
                user: user
            });

            return userInfo
        } catch (e) {
            console.error(e)
            return res.status(500).json({ error: "Facebook Login Unknown Error" })
        }
    }

}
