import express from 'express';
import { CommentService } from "../_services/CommentService";

export class CommentController {
    public constructor(private commentService: CommentService) { }

    public getCommentData = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.commentService.getComment(req.params.id);

            // console.log(result)
            return res.json(result)
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "CommentData Unknown Error" })
        }
    }

    public postCommentData = async (req: express.Request, res: express.Response) => {
        try {
            const { text } = req.body;

            if(!text){
                return res.status(400).json({error:"請輸入文字"})
            }

            await this.commentService.postComment(text, req.user?.id, req.params.id)


            const result = res.json({
                text: text,
              
            })

            return result
        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "PostCommentData Unknown Error" })
        }
    }
}