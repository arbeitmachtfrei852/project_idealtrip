import express from 'express';
import { CreateJourneyService } from '../_services/CreateJourneysService'


export class CreateJourneyController {
    public constructor(private createJourneyService: CreateJourneyService) { }

    public addJourney = async (req: express.Request, res: express.Response) => {
        try {
            const { location_name, address, lat, lng, note, start_time } = req.body

            if (!location_name || !address || !lat || !lng || !start_time) {
                return res.status(400).json({ error: "please fill in" })
            }

            const id = await this.createJourneyService.postJourney(
                req.params.tripId,
                req.params.dayId as any,
                location_name,
                address,
                lat,
                lng,
                note,
                start_time,
            )

            return res.json({
                id: id,
                location_name: location_name,
                trip_id: req.params.tripId,
                day: req.params.dayId,
                address: address,
                lat: lat,
                lng: lng,
                note: note,
                start_time: start_time
            })


        } catch (e) {
            console.error(e);
            return res.status(500).json({ error: "CreateJouney" })
        }
    }
}