import { TripService } from "../_services/TripService";
import express from "express";

export class TripController {
  public constructor(private tripService: TripService) { }


  public tripData = async (req: express.Request, res: express.Response) => {
    try {
      const result = await this.tripService.getTrip(req.user?.id);
      res.status(200).json(result);
      return result;
    } catch (e) {
      console.error(e);
      return res.status(500).json({ error: "TripData Unknown Error" });
    }
  };

  
  public addTrip = async (req: express.Request, res: express.Response) => {
    try {
      const { tripName, startDate, endDate } = req.body;
      const currentUserId = req.user?.id;


      if(!tripName|| !startDate || !endDate) {
        return res.status(400).json({error: "請確保輸入所有旅程資料"})
      }

      const id = await this.tripService.addTrip(tripName, startDate, endDate, currentUserId);
   
      if(endDate < startDate) {
        return res.status(401).json({error: "出發日期 或 回程日期 錯誤"})
      }

      const result = res.json({
        id: id,
        tripName: tripName,
        startDate: startDate,
        endDate: endDate,
        ownerId: currentUserId,
        trip_id: id
      });

      return result
    } catch (e) {
      console.error(e);
      return res.status(500).json({ error: "AddTrip Unknown Error" });
    }
  };


  public deleteTrip = async (req: express.Request, res: express.Response) => {
    try {
      await this.tripService.deleteTrip(req.params.id);
      return res.json({ success: true });
    } catch (e) {
      console.error(e);
      return res.status(500).json({ error: "DeleteTrip Unknown Error" });
    }
  };


  public addFriend = async (req: express.Request, res: express.Response) => {
    try {
      const { email } = req.body;

      const id = await this.tripService.addFriendsToTrip(email, req.params.id);

      const result = res.json({
        ownerId: id,
      })

      return result
    } catch (e) {
      console.error(e);
      return res.status(500).json({ error: "addFriend Unknown Error" });
    }
  };


  public getAddedTripCurrentUser = async (req:express.Request, res:express.Response) =>{
    try{
      const result = await this.tripService.listTripCurrentUser(req.params.id);
      return res.status(200).json(result)
    }catch(e){
      console.error(e);
      return res.status(500).json({ error: "getAddedTripCurrentUser Unknown Error" });
    }
  }


}


