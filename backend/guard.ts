import express from "express";
import { UserService } from "./_services/UserService";
import { Bearer } from 'permit'
import jwtSimple from 'jwt-simple'
import jwt from './jwt'

export const createIsLoggedIn = (permit: Bearer, userService: UserService) => {
    return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const token = permit.check(req);

            if (!token) {
                return res.status(401).json({ error: '2Permission Denied' })
            }

            const payload = jwtSimple.decode(token, jwt.jwtSecret)
            const user = await userService.getUserById(payload.id)


            if (!user) {
                return res.status(401).json({ error: '1Permission Denied' })
            }

            const { password, ...userWithoutPassword } = user;

            req.user = userWithoutPassword;

            return next();
        } catch (e) {
            console.error(e);
            return res.status(401).json({ error: '3Permission Denied' })
        }

    }
}