import Knex from "knex";

export class IndividualMaterialService{
    public constructor(private knex:Knex){}

    public async getMaterials(tid:string,uid:number|undefined){
        const result = await this.knex.transaction(async (trx)=>{
            const tripId = await trx
            .select("trips.id")
            .from("trips")
            .where("trips.id",tid)

            return await trx
            .select("individual_materials.id",
                    "individual_materials.item",
                    "individual_materials.user_id",
                    "individual_materials.trip_id",
                    "individual_materials.checked")
            .from("individual_materials")
            .innerJoin("trips","individual_materials.trip_id","trips.id")
            .innerJoin("users","individual_materials.user_id","users.id")
            .where("trip_id",tripId[0].id)
            .andWhere('user_id',uid)
            .orderBy("individual_materials.id", "desc");
        })
        return result 
    } 

    public async addMaterials(tid:string,uid:number|undefined, item:string, checked:boolean){
        const result = await this.knex.transaction(async (trx)=>{
            const tripId = await trx
            .select("trips.id")
            .from("trips")
            .where("trips.id", tid)

            const indId =  await trx
            .insert({
                item: item,
                checked: checked,
                trip_id: tripId[0].id,
                user_id: uid,
            }).into("individual_materials").returning('id')
            console.log(indId[0])
            return indId
            
        })
        return result
    }   

    public async checkedMaterials(item_id:string, checked:boolean){
        const result = await this.knex('individual_materials')
        .where('id',item_id)
        .update("individual_materials.checked", true)
        
        return result
        
    }

    public async delMaterials(item_id:string){
        const result = await this.knex('individual_materials')
            .where('id',item_id)
            .del()
            return result
    }

}