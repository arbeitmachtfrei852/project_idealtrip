import Knex from "knex";
// import { hashPassword } from "../hash";

export class UserService {
    getUser(username: any) {
        throw new Error("Method not implemented.");
    }
    public constructor(private knex: Knex) { }

    public async getUserByEmail(email: string) {
        return (await this.knex.raw(`SELECT * FROM users WHERE email = ?`, [email])).rows[0];
    }

    public async getUserById(id: number) {
        return (await this.knex.raw(`SELECT * FROM users WHERE id = ?`, [id])).rows[0];
    }

    public async getUserByGoogleId(id: number) {
        // const result = await this.knex('users').where('google_id', id)
        const result = (await this.knex.raw(`SELECT * FROM users WHERE google_id = ?`, [id])).rows[0]
        console.log(result)
        return result
    }

    public async getUserByFacebookId(id: number) {
        const result = (await this.knex.raw(`SELECT * FROM users WHERE facebook_id = ?`, [id])).rows[0]
        console.log(result)
        return result
        // return (await this.knex('users').where("facebook_id", id))
    }

    public async registerMatchEmail(email: string) {
        const result = (await this.knex.select('email').from('users').where('email', email));
      //  console.log(result[0].email)
        return result
    }

    public async createFacebookUser(email:string, facebookId:string, password: string){
        return (await this.knex.raw(`INSERT INTO users (email, facebook_id, password) VALUES (?, ?, ?) RETURNING id`, [
            email, facebookId, password
        ])).rows[0]
    }

    public async createGoogleUser(email: string, googleId: string, password: string) {
        return (await this.knex.raw(`INSERT INTO users (email, google_id, password) VALUES (?, ?, ?) RETURNING id`, [
            email, googleId, password
        ])).rows[0]

    }

    public async createUser(email:string, password: string){
        const result = await this.knex.insert({
            email:email,
            password: password
        }).into('users').returning('id')
        return result
    }
}