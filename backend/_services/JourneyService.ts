import Knex from "knex";

export class JourneyService {
    public constructor(private knex: Knex) { }

    public async getExactlyTrip(trip_id: string, user_id: number | undefined) {
        return await this.knex
            .select(
                "trips.trip_name",
                "trips.start_date",
                "trips.end_date",
            )
            .from("trips")
            .leftOuterJoin("users_trips", "users_trips.trip_id", "trips.id")
            .where("users_trips.owner_id", user_id)
            .where("trips.id", trip_id)
    }

    public async getJourney(trip_id: string) {
        return await this.knex("journeys")
            .where("journeys.trip_id", trip_id)
            .orderBy('start_time','asc')
            .returning("id")
    }

    public async delJourney(journey_id:string){
        const result=  await this.knex('journeys')
            .where('id', journey_id)
            .del()
            console.log(`service: ${result}`)
        return result
    }
    

    // for Direction uses

    public async getExactlyJourney(trip_id: string, journey_id: string){
        return await this.knex.select('*').from('journeys').where('journeys.trip_id', trip_id).andWhere('journeys.id', journey_id)
    }


}