import Knex from "knex";

export class GroupMaterialService{
    public constructor(private knex:Knex){}

    public async getMaterials(id:string){
        const result = await this.knex.transaction(async (trx)=>{
            const tripId = await trx
            .select("trips.id")
            .from("trips")
            .where("trips.id", id)
        
            return await trx
            .select(
                "group_materials.id",
                "group_materials.item",
                "group_materials.assignee",
                "group_materials.checked",
                "group_materials.trip_id"
            )
            .from('group_materials')
            .innerJoin("trips","group_materials.trip_id","trips.id")
            .where("trip_id", tripId[0].id)
            .orderBy("group_materials.id", "desc");
        })
        return result
    }

    public async addMaterials(trip_id:string, item:string, assignee:string|null, checked:boolean){
        const result = await this.knex.transaction(async (trx)=>{
            const tripId = await trx
            ("trips")
            .where("trips.id", trip_id)

            const gpId = await trx
            .insert({
                trip_id: tripId[0].id,
                item: item,
                assignee:assignee,
                checked: checked
            })
            .into('group_materials').returning('id')

            return gpId[0]
        })
        return result
    }

    public async delMaterials(item_id:string){
        const result = await this.knex('group_materials')
            .where('id', item_id)
            .del()
            return result
    }
    
    
}