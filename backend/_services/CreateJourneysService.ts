import Knex from 'knex';


export class CreateJourneyService{
    public constructor(private knex:Knex){}

    public async postJourney(
        trip_id:string, 
        day:Date,
        location_name:string,
        address: string,
        lat:number,
        lng:number,
        note:string,
        start_time:string, 
        
      
        ){const result = await this.knex.transaction(async (trx)=>{
            const tripId = await trx('trips').where("trips.id", trip_id)
            
            const journeyId = await trx
            .insert({
                trip_id: tripId[0].id,
                note: note,
                day:day,
                location_name:location_name,
                address: address,
                lat:lat,
                lng: lng,
                start_time: start_time
            })
            .into("journeys").returning("id")

            return journeyId
        })
        return result
        }
}