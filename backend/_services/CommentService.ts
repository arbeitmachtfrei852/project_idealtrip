import Knex from "knex";

export class CommentService {
    public constructor(private knex: Knex) { }

    public async getComment(tripId: string) {
        const result = await this.knex
            .select("comments.text", "users.email")
            .from("comments")
            .leftOuterJoin("users", "users.id", "comments.user_id")
            .where("trip_id", tripId)
            .orderBy("comments.id","desc")
        return result
    }

    public async postComment(text: string, userId: number | undefined, tripId: string) {
        const result = this.knex.transaction(async (trx) => {
            await trx
                .insert({
                    text: text,
                    user_id: userId,
                    trip_id: tripId,
                })
                .into("comments")
        })
        return result
    }
}