import Knex from "knex";

export class TripService {
  public constructor(private knex: Knex) { }

  public async getTrip(id: number | undefined) {
    const result = await this.knex
      .select(
        "trips.id",
        "trips.trip_name",
        "trips.start_date",
        "trips.end_date",
        "users_trips.owner_id",
        "users_trips.trip_id"
      )
      .from("trips")
      .leftOuterJoin("users_trips", "users_trips.trip_id", "trips.id")
      .where("owner_id", id)
      .orderBy("users_trips.trip_id", "desc");
    //   console.log(result)
    return result;
  }

  public async addTrip(tripName: string, startDate: Date, endDate: Date, owner_id: number | undefined) {
    const id = await this.knex.transaction(async (trx) => {
      const tripIds = await trx.insert({
        trip_name: tripName,
        start_date: startDate,
        end_date: endDate,
      })
        .into("trips")
        .returning("id");

      await trx.insert({
        owner_id: owner_id,
        trip_id: tripIds[0],
      })
        .into("users_trips");
      return tripIds[0];
    });
    return id;
  }

  public async deleteTrip(trip_id: string) {
    const id = await this.knex.transaction(async (trx) => {
      const tripId = await trx("users_trips")
        .where("trip_id", trip_id)
        .del()
        .returning("trip_id");

      await trx("group_materials").where("trip_id", tripId[0]).del()
      await trx("individual_materials").where("trip_id", tripId[0]).del()
      await trx("comments").where("trip_id", tripId[0]).del();
      await trx("journeys").where("trip_id", tripId[0]).del();
      await trx("trips").where("id", tripId[0]).del();

      return tripId[0];
    });
    return id;
  }

  public async addFriendsToTrip(email: string, tripIds: string) {
    const friendId = this.knex.transaction(async (trx) => {
      const userId = await trx
        .select("id", "email")
        .from("users")
        .where("email", email);

      await trx
        .insert({
          owner_id: userId[0].id,
          trip_id: tripIds,
        })
        .into("users_trips");

      return userId[0];
    });
    return friendId;
  }

  public async listTripCurrentUser(trip_id: string) {
    return await this.knex
      .select("users.email")
      .from("trips")
      .leftOuterJoin("users_trips", "users_trips.trip_id", "trips.id")
      .leftOuterJoin("users", "users.id", "users_trips.owner_id")
      .where("trip_id", trip_id);
  }

}
