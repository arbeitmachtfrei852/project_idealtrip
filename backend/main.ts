import express from 'express'
import dotenv from 'dotenv'
import Knex from 'knex'
import bodyParser from 'body-parser'
import { UserService } from './_services/UserService';
import { AuthController } from './_controllers/AuthController';
import { Bearer } from 'permit'
import { createIsLoggedIn } from './guard';
import cors from 'cors'
import { UserController } from './_controllers/UserController';
import { TripService } from './_services/TripService';
import { TripController } from './_controllers/TripController';
import { GroupMaterialController } from './_controllers/GroupMaterialController';
import { GroupMaterialService } from './_services/GroupMaterialService';
import { CommentService } from './_services/CommentService';
import { CommentController } from './_controllers/CommentController';
import { JourneyService } from './_services/JourneyService';
import { JourneyController } from './_controllers/JourneyController';
import { IndividualMaterialService } from './_services/IndividualMaterialService';
import { IndividualMaterialController } from './_controllers/IndividualMaterialControllers';
import {CreateJourneyController} from './_controllers/CreateJourneyController';
import {CreateJourneyService} from './_services/CreateJourneysService';

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

dotenv.config();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())

declare global {
  namespace Express {
    interface Request {
      user?: {
        id: number;
        email: string;
      }
    }
  }
}

const userService = new UserService(knex);
const authController = new AuthController(userService);
const userController = new UserController(userService);
const tripService = new TripService(knex);
const tripController = new TripController(tripService);
const commentService = new CommentService(knex);
const commentController = new CommentController(commentService)
const journeyService = new JourneyService(knex);
const journeyController = new JourneyController(journeyService)
const createJourneyService = new CreateJourneyService(knex)
const createJourneyController = new CreateJourneyController(createJourneyService)

const permit = new Bearer({
  query: "access_token"
})

const isLoggedIn = createIsLoggedIn(permit, userService)

//jose
app.get('/user', isLoggedIn, userController.getCurrentUser)
app.post('/login', authController.login);
app.post('/loginGoogle', authController.googleLogin)
app.get('/trip', isLoggedIn, tripController.tripData)
app.post('/addTrip', isLoggedIn, tripController.addTrip)
app.put('/trip/:id/deletetrip', isLoggedIn, tripController.deleteTrip)
app.post('/trip/:id/addFriend',isLoggedIn,tripController.addFriend)
app.get('/trip/:id/list', isLoggedIn,tripController.getAddedTripCurrentUser)
app.get('/trip/:id/comment', isLoggedIn,commentController.getCommentData)
app.post('/trip/:id/post/comment', isLoggedIn,commentController.postCommentData)
app.get('/journey/:id/trip', isLoggedIn,journeyController.getExactlyTripData)
app.get('/journey/:id/', isLoggedIn,journeyController.getJourneyData)
app.get('/journey/:tripId/:journeyItemId', isLoggedIn,journeyController.getExactlyJourneyData)

//ethan
const groupMaterialService = new GroupMaterialService(knex)
const groupMaterialController = new GroupMaterialController(groupMaterialService);
const indMaterialService = new IndividualMaterialService(knex)
const indMaterialsController = new IndividualMaterialController(indMaterialService)
app.get('/groupMaterials/:id/getGroupMaterial',isLoggedIn, groupMaterialController.getMaterials)
app.post('/groupMaterials/:id/addGroupMaterial',isLoggedIn,groupMaterialController.addMaterials)
app.put('/groupMaterials/:id/deleteGroupMaterial',isLoggedIn,groupMaterialController.delMaterials)
app.get('/individualMaterials/:id/getIndMaterial',isLoggedIn,indMaterialsController.getMaterials)
app.put('/individualMaterials/:id/checkedIndMaterial',isLoggedIn,indMaterialsController.checkedMaterials)
app.post('/individualMaterials/:id/addIndMaterial',isLoggedIn,indMaterialsController.addMaterials)
app.put('/individualMaterials/:id/deleteIndMaterial',isLoggedIn,indMaterialsController.deleteMaterials)
app.post('/createjourney/:tripId/:dayId/addJourney',isLoggedIn,createJourneyController.addJourney)
app.post('/loginFacebook',authController.loginFacebook)
app.put("/journey/:id/deleteJourney",isLoggedIn,journeyController.delJourneyData)
app.post('/register',authController.register)

const PORT = process.env.PORT || 8080
app.listen(PORT, () => {
  console.log('Listening at port ' + PORT)
})