import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("users_trips");
    if (!hasTable) {
        return knex.schema.createTable("users_trips", (table) => {
            table.increments();
            table.integer("owner_id").unsigned();
            table.foreign("owner_id").references("users.id")
            table.integer("trip_id").unsigned();
            table.foreign("trip_id").references("trips.id")
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("users_trips")
}

