import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("individual_materials");
    if (!hasTable) {
        return knex.schema.createTable("individual_materials", (table) => {
            table.increments();
            table.string("item");
            table.boolean("checked");
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.integer("trip_id").unsigned();
            table.foreign("trip_id").references("trips.id");
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("individual_materials")
}

