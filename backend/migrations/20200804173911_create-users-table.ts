import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("users");
    if (!hasTable) {
        return knex.schema.createTable("users", (table) => {
            table.increments();
            table.string("email");
            table.string("password");
            table.timestamps(false, true)
            table.string('facebook_id').unique()
            table.string('google_id').unique()
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("users")
}

