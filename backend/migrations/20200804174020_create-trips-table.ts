import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("trips");
    if (!hasTable) {
        return knex.schema.createTable("trips", (table) => {
            table.increments();
            table.string("trip_name");
            table.date("start_date");
            table.date("end_date");
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("trips")
}

