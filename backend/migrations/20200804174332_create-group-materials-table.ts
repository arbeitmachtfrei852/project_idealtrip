import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("group_materials");
    if (!hasTable) {
        return knex.schema.createTable("group_materials", (table) => {
            table.increments();
            table.string("item");
            table.boolean("checked");
            table.string("assignee");
            table.integer("trip_id").unsigned();
            table.foreign("trip_id").references("trips.id");
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("group_materials")
}

