import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("journeys");
    if (!hasTable) {
        return knex.schema.createTable("journeys", (table) => {
            table.increments();
            table.date("day");
            table.string("location_name");
            table.string("address");
            table.float("lat");
            table.float("lng");
            table.time("start_time")
            table.string("note");
            table.integer("trip_id").unsigned();
            table.foreign("trip_id").references("trips.id")
        })
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("journeys")
}

