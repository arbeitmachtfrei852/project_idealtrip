import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { connectRouter, routerMiddleware, RouterState, RouterAction } from "connected-react-router";
import { createBrowserHistory } from "history";
import thunk, { ThunkDispatch as OldThunkDispatch } from "redux-thunk";
import { MapState, mapReducer } from "./redux/map/reducer";
import { AuthState, authReducer } from "./redux/auth/reducer";
import { AuthActions } from "./redux/auth/actions";
import { TripState, tripReducer } from "./redux/trip/reducer";
import { TripActions } from "./redux/trip/actions";
import { MapActions } from "./redux/map/actions";
import { GroupMaterialActions } from "./redux/gpMaterial/actions";
import { GroupMaterialsState, GroupMaterialReducer } from "./redux/gpMaterial/reducer";
import { CommentState, commentReducer } from "./redux/comment/reducer";
import { CommentActions } from "./redux/comment/actions";
import { JourneyState, journeyReducer } from "./redux/journey/reducer";
import { JourneyActions } from "./redux/journey/actions";
import { IndMaterialsState, IndMaterialReducer } from './redux/indMaterial/reducers';
import { IndMaterialsAction } from './redux/indMaterial/actions'
import { createJourneyReducer, CreateJourneyState } from './redux/createJourney/reducer'
import { CreateJourneyActions } from './redux/createJourney/actions'

export const history = createBrowserHistory();

// 1. RootState
export interface RootState {
    trip: TripState,
    auth: AuthState,
    map: MapState,
    groupMaterial: GroupMaterialsState,
    indMaterial: IndMaterialsState,
    comment: CommentState,
    journey: JourneyState,
    createJourney: CreateJourneyState,
    router: RouterState
}

export type RootActions =
    RouterAction | AuthActions | TripActions |
    MapActions | GroupMaterialActions | CommentActions |
    JourneyActions | IndMaterialsAction | CreateJourneyActions;


//3. CombineREducer
const reducers = combineReducers<RootState>({
    trip: tripReducer,
    auth: authReducer,
    map: mapReducer,
    groupMaterial: GroupMaterialReducer,
    comment: commentReducer,
    journey: journeyReducer,
    createJourney: createJourneyReducer,
    indMaterial: IndMaterialReducer,
    router: connectRouter(history)
})

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, RootActions, {}, {}>(reducers, composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
))