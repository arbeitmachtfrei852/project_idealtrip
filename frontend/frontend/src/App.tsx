import React, { useState, useEffect } from "react";
import "./App.scss";
import { Switch, Route } from "react-router-dom";
import { LoginPage } from "./pages/LoginPage";
import { DirectionPage } from "./pages/DirectionPage";
import { HomePage } from "./pages/HomePage";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "./store";
import { restoreLogin } from "./redux/auth/actions";
import { PrivateRoute } from "./PrivateRoute";
import { IonApp } from "@ionic/react";
import { MaterialPage } from "./pages/MaterialPage";
import { CommentPage } from "./pages/CommentPage";
import { JourneyPage } from "./pages/JourneyPage";
import { GroupMaterials} from './pages/GroupMaterialsPage'
import { IndividualMaterials } from "./pages/IndividualMaterialsPage";
import { CreateJourney } from "./pages/CreateJourneyPage";

function App() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(restoreLogin());
  }, [dispatch])

  return (
    <IonApp>

      {isAuthenticated === null ? <div>Loading...</div> :
        <>
          <Switch>
            <PrivateRoute path="/comment"><CommentPage /></PrivateRoute>
            <PrivateRoute path="/material"><MaterialPage /></PrivateRoute>
            <PrivateRoute path="/direction"><DirectionPage /></PrivateRoute>
            <PrivateRoute path='/individualMaterials'><IndividualMaterials/></PrivateRoute>
            <PrivateRoute path="/journey"><JourneyPage /></PrivateRoute>
            <PrivateRoute path='/groupmaterials'><GroupMaterials/></PrivateRoute>
            <PrivateRoute path='/createjourney'><CreateJourney /></PrivateRoute>
            {isAuthenticated ? 
            <Route path="/home"><HomePage /></Route> 
            : 
            <Route path="/login"><LoginPage /></Route>
            }
            <Route path="/"><LoginPage /></Route>
          </Switch>
        </>
      }

    </IonApp>
    
  );
}

export default App;
