import React, { useState } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { RootState } from '../store'
import PlacesAutocomplete from 'react-places-autocomplete';
import {
    geocodeByAddress, geocodeByPlaceId, getLatLng,
} from 'react-places-autocomplete';
import GoogleMapReact from 'google-map-react';
import useReactRouter from 'use-react-router';
import { postJourney } from '../redux/createJourney/actions';
import { IonApp, IonHeader, IonToolbar, IonTitle, IonButton, IonImg, IonContent, IonLabel, IonInput, IonItem, IonList, IonDatetime } from '@ionic/react';
import { Menu } from './HomePage';
import { Link } from 'react-router-dom';
import back from "../icon/back.svg";
import "../css/createJourney.scss";
import { push } from 'connected-react-router';
import { Marker } from './DirectionPage';

export function CreateJourney() {

    const dispatch = useDispatch()

    // const groupItem = useSelector((state: RootState) => state.createJourney.journeyInfo)

    const [latLng, setLatLng] = useState({ lat: 22.3193, lng: 114.1694 })
    const [address, setAddress] = useState<string>("")
    const [location_name, setLocationName] = useState("")
    const [note, setNote] = useState("")
    const [start_time, setStartTime] = useState("")

    const lat = latLng.lat
    const lng = latLng.lng

    const { location } = useReactRouter();
    const tripId = location.search;
    const params = new URLSearchParams(tripId);
    const trip_id = params.get('trip_id');
    const day = params.get('journey_date')
    const user_id = params.get('user_id')
    const email = params.get('email')

    let startTime = new Date(start_time)
    let adjustedStartTime = startTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    console.log(`ad : ${adjustedStartTime}`)

    return (
        <IonApp>
            <Menu />
            <IonHeader id="main-content">
                <IonToolbar mode="ios" color="primary" className="journey-toolbar-button-container">
                    <IonTitle className="toolbar-title">Create Journey</IonTitle>
                    <Link to={`/journey?user_id=${user_id}&email=${email}&trip_id=${trip_id}`} slot="start">
                        <IonButton><IonImg src={back}></IonImg></IonButton>
                    </Link>

                </IonToolbar>
            </IonHeader>

            <IonContent>
                <form onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(postJourney(trip_id, day as any, location_name, address, lat, lng, note, adjustedStartTime))
                    await dispatch(push(`/journey?user_id=${user_id}&email=${email}&trip_id=${trip_id}`))
                }}>

                    <IonItem>
                        <IonLabel position="floating">Topic Name</IonLabel>
                        <IonInput type="text" value={location_name} onIonChange={(e) => setLocationName(e.detail.value!)} > </IonInput>
                    </IonItem>

                    <PlacesAutocomplete
                        value={address}
                        onChange={address => setAddress(address)}
                        onSelect={address => {
                            geocodeByAddress(address)
                                .then(results => getLatLng(results[0]))
                                .then(latLng => {
                                    setLatLng(latLng);
                                    console.log(latLng)
                                })
                                .catch(error => console.error("error", error))
                        }}>
                        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                            <div>
                                <IonItem lines="none">
                                    <div className="input-address-container">
                                        <input {...getInputProps({
                                            placeholder: "Address",
                                            className: "location-search-input",
                                        })
                                        }
                                        /></div>

                                </IonItem>
                                <div className="autocomplete-dropdown-container">
                                    {loading && <div>Loading...</div>}
                                    {suggestions.map(suggestion => {
                                        const className = suggestion.active ?
                                            "suggestion-item--active" :
                                            "suggestion-item";
                                        const style = suggestion.active ?
                                            { backgroundColor: "#fafafa", cursor: "pointer" }
                                            : { backgroundColor: "#ffffff", cursor: "pointer" };

                                        return (
                                            <div className="input-suggestion"
                                                {...getSuggestionItemProps(suggestion, {
                                                    style,
                                                })}
                                                onMouseDown={() => {
                                                    setAddress(suggestion.description)
                                                }}
                                                onMouseEnter={() => {
                                                    setAddress(suggestion.description)
                                                }}
                                            >
                                                <div className="autocomplete-dropdown-info">{suggestion.description}</div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        )}
                    </PlacesAutocomplete>

                    <div style={{ height: '35vh', width: '100vw' }}>
                        <GoogleMapReact bootstrapURLKeys={{ key: `${process.env.REACT_APP_API_KEY_GOOGLE_MAPS}` }}
                            center={latLng} zoom={15}>

                               {address &&<Marker lat={lat} lng={lng}/>}  
                        </GoogleMapReact>
                    </div>
                    <IonLabel className="latLng-text-container">Lat: {lat} | Lng: {lng}</IonLabel>

                    <IonItem>
                        <IonLabel position="floating">Record</IonLabel>
                        <IonInput type="text" value={note} onIonChange={(e) => setNote(e.detail.value!)} />
                    </IonItem>

                    <IonItem>
                        <IonLabel position="floating">Start Time</IonLabel>
                        <IonDatetime pickerFormat="HH:mm" displayFormat="HH:mm" value={start_time} onIonChange={(e) => setStartTime(e.detail.value!)} />
                    </IonItem>

                    <div className="added-journey-button-container">
                        <IonButton type="submit">Add</IonButton>
                    </div>

                </form>

            </IonContent>
        </IonApp>
    )
}