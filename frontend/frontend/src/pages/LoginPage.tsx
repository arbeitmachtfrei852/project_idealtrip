import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Image from "react-bootstrap/Image";
import emailIcon from "../icon/email.svg";
import lockIcon from "../icon/lock.svg";
import { IonSegmentButton } from "@ionic/react";
import { useFormState } from "react-use-form-state";
import "../css/Login.scss";
import { useDispatch, useSelector } from "react-redux";
import { login, loginFailed, loginGoogle, loginFacebook, registerFail, register } from "../redux/auth/actions";
import { RootState } from "../store";
import { IonToast } from "@ionic/react";
import GoogleLogin from 'react-google-login';
import ReactFacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login'

export function LoginPage() {
  const [login, setLogin] = useState(true);
  const [register, setRegister] = useState(false);
  const dispatch = useDispatch()

  function showRegisterBox() {
    setRegister(true);
    setLogin(false);
    dispatch(loginFailed(null))
  }

  function showLoginBox() {
    setRegister(false);
    setLogin(true);
    dispatch(registerFail(null))
  }

  return (
    <div className="login-bg">

      <div className="box-container">
        <div className="box-controller">
          <div
            className={
              "controller " + (login ? "selected-controller-login" : "")
            }
          >
            <IonSegmentButton onClick={showLoginBox}>LOGIN</IonSegmentButton>

          </div>
          <div
            className={
              "controller " + (register ? "selected-controller-register" : "")
            }
          >
            <IonSegmentButton onClick={showRegisterBox}>
              Register
            </IonSegmentButton>
          </div>

        </div>
        {login && <LoginBox />}
        {register && <RegisterBox />}

      </div>

    </div>
  );
}

function LoginBox() {
  const [formState, { text, password }] = useFormState();
  const dispatch = useDispatch();
  const errorMessage = useSelector((state: RootState) => state.auth.error);
  const [errorToast, setErrorToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    if (errorMessage) {
      setErrorMsg(errorMessage)
      setErrorToast(true)
      return
    }
  }, [errorMessage, errorMsg])

  return (
    <div>
      <form
        className="login-container"
        onSubmit={async (event) => {
          event.preventDefault();
          await dispatch(loginFailed(null))
          await dispatch(login(formState.values.email, formState.values.password)
          );
        }}
      >
        <div className="login-box">
        <div className="login-title">Login</div>
          <Image src={emailIcon} />
          <Form.Group controlId="formBasicEmail">
            <Form.Control
              className="login-input"
              placeholder="email"
              {...text("email")}
              type="email"
            />
          </Form.Group>
          <Image src={lockIcon} />
          <Form.Group controlId="formBasicPassword">
            <Form.Control
              className="login-input"
              placeholder="password"
              {...password("password")}
              type="password"
            />
          </Form.Group>
          <div className="login-button-container">
            <Button
              variant="primary"
              type="submit"
              onClick={async () => {
                await dispatch(loginFailed(null))
                setErrorToast(false)
              }}>Login
            </Button>
          </div>

          <div className="social-login-line"></div>
          <SocialLogin />


          <IonToast
            mode="ios"
            isOpen={errorToast}
            onDidDismiss={() => setErrorToast(false)}
            message={errorMsg}
            duration={1200}
            color="danger"
          />
        </div>
      </form>
    </div>
  );
}

function RegisterBox() {
  const [formState, { text, password }] = useFormState();
  const dispatch = useDispatch();
  const errorMessage = useSelector((state: RootState) => state.auth.error);
  const [errorToast, setErrorToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    if (errorMessage) {
      setErrorMsg(errorMessage)
      setErrorToast(true)
      return
    }
  }, [errorMessage, errorMsg])

  return (
    <div>
      <form className="register-container"
        onSubmit={async (event) => {
          event.preventDefault();
          await dispatch(registerFail(null))
          await dispatch(register(formState.values.email, formState.values.password, formState.values.confirmedpassword))
        }}>
        <div className="register-box">
          <div className="register-title">Register</div>

          <Image src={emailIcon} />
          <Form.Group controlId="formBasicEmail">
            <Form.Control
              className="register-input"
              placeholder="email"
              {...text("email")}
              type="email"
            />
          </Form.Group>
          <Image src={lockIcon} />
          <Form.Group controlId="formBasicPassword">
            <Form.Control
              className="register-input"
              placeholder="password"
              {...password("password")}
              type="password"
            />
          </Form.Group>

          <Form.Group controlId="formBasicConfirmedPassword">
            <Form.Control
              className="register-input"
              placeholder="confirm password"
              {
                ...password("confirmedpassword")}
                type="password"
            />
          </Form.Group>

          <div className="register-button-container">
            <Button variant="danger" type="submit"
              onClick={async () => {
                await dispatch(registerFail(null))
                setErrorToast(false)
              }}>
              Register
            </Button>
          </div>

          <IonToast
            mode="ios"
            isOpen={errorToast}
            onDidDismiss={() => setErrorToast(false)}
            message={errorMsg}
            duration={1200}
            color="danger"
          />
        </div>
      </form>
    </div>
  );
}


function SocialLogin() {
  const dispatch = useDispatch();

  let responseGoogle = (response: any) => {
    if (response.accessToken) {
      dispatch(loginGoogle(response.accessToken))
      console.log(response);
    }
    console.log(response);
    return null;
  }

  const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
    if (userInfo.accessToken) {
      dispatch(loginFacebook(userInfo.accessToken));
      console.log(userInfo)
    }
    return null;
  }

  return (
    <div className="social-login-container">

      <div className="social-login">
        <GoogleLogin
          clientId={process.env.REACT_APP_GOOGLE_APP_ID || ''}
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={'single_host_origin'}
          disabled={false}
          buttonText=""
          className="my-google-button-class"
        />
      </div>

      <div className="social-login">
        <ReactFacebookLogin
          appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
          autoLoad={false}
          fields="name,email,picture"
          onClick={() => { }}
          callback={fBCallback}
          textButton=""
          icon="fa-facebook"
          cssClass="my-facebook-button-class"
        />
      </div>
    </div>
  )
}