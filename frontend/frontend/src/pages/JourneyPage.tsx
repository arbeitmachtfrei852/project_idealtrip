import React, { useEffect, useState } from 'react';
import { IonApp, IonHeader, IonToolbar, IonButton, IonTitle, IonContent, IonImg, IonItem, IonCard, IonCardTitle, IonCardSubtitle, IonFooter, IonLabel, IonCardContent, IonCol, IonRow,  IonGrid,  IonToggle } from '@ionic/react';
import { Menu } from './HomePage';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import moment from 'moment';
import Moment from 'react-moment';
import useReactRouter from 'use-react-router';
import { getExactlyTrip, getJourney, delJourney,  } from '../redux/journey/actions';
import { Link } from 'react-router-dom';
import "../css/Journey.scss";
import checklist from "../icon/checklist.svg";
import back from "../icon/back.svg";
import journeyDel from "../icon/journeyDelete.svg";
import journeyGps from "../icon/journeyGps.svg";
import { loadExactlyJourney } from '../redux/map/actions';
// import { DirectionPage } from './DirectionPage';
import { push } from 'connected-react-router';
import journeyGpsWhite from "../icon/journeyGps-white.svg";

export function JourneyPage() {
    const tripInfo = useSelector((state: RootState) => state.journey.exactlyTrip)
    const journeyInfo = useSelector((state: RootState) => state.journey.journey)

    const [on, setOn] = useState(false);
    // console.log(on)

    const dispatch = useDispatch();
    const { location } = useReactRouter();
    const tripId = location.search
    const params = new URLSearchParams(tripId)
    const trip_id = params.get('trip_id')
    const user_id = params.get('user_id')
    const email = params.get('email')


    let today: any = new Date().toISOString().split('T')

    let now: any = moment();
    let currentHour = now.get('hour')

    useEffect(() => {
        dispatch(getExactlyTrip(trip_id));
        dispatch(getJourney(trip_id))
    },[])


    return (
        <IonApp>
            <Menu />
            <IonHeader id="main-content" mode="ios">
                <IonToolbar color="primary" className="journey-toolbar-button-container">
                    <IonTitle className="toolbar-title">Journey</IonTitle>
                    <Link to={`/home?email=${email}`} slot="start">
                        <IonButton><IonImg src={back}></IonImg></IonButton>
                    </Link>
                    <Link  to={`/groupmaterials?trip_id=${trip_id}&user_id=${user_id}&email=${email}`} slot="end">
                        <IonButton><IonImg src={checklist}></IonImg></IonButton>
                    </Link>
                    <IonToggle mode="ios" slot="end" onIonChange={()=>setOn(!on)}/>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                {tripInfo.map(data => (
                    <div className="journey-trip-info-container">
                        <div className="journey-trip-info">
                            <IonCardTitle>{data.trip_name}</IonCardTitle>
                            <IonCardSubtitle>Departure Date：{moment(data.start_date).format('YYYY-MM-DD')}</IonCardSubtitle>
                            <IonCardSubtitle>Return Date：{moment(data.end_date).format('YYYY-MM-DD')} </IonCardSubtitle>
                            {/* <IonCardSubtitle><Moment duration={moment(data.start_date).format('YYYY-MM-DD')}
                                date={moment(data.end_date).format('YYYY-MM-DD')}
                            /></IonCardSubtitle> */}
                        </div>
                    </div>
                ))}


               
                {journeyInfo.map((dateResult, day) => (

                    <div className="journey-card-container">

                        <IonItem mode="ios" className={today[0] === dateResult.allDate as any ? "highlight-that-day" : ""}>
                            <div className="date-card-title-container">
                                <IonLabel>Day{day + 1}</IonLabel>
                                <IonLabel>{dateResult.allDate}</IonLabel>
                            </div>
                        </IonItem>
                        <IonGrid>
                            <IonRow>
                                {dateResult.journey.map(result => (
                                    <IonCol size="12" sizeMd="6" sizeLg="4">
                                        <div className="journey-info-container">
                                            <form onSubmit={async (event) => {
                                                event.preventDefault();
                                                await dispatch(loadExactlyJourney(result.trip_id, result.id))
                                                await dispatch(push(`/direction?email=${email}&trip_id=${trip_id}&address=${result.address}&lat=${result.lat}&lng=${result.lng}`))
                                            }}>

                                                <IonCard mode="ios" className={today[0] == dateResult.allDate && currentHour as any == result.start_time.slice(0, 2) ? "highlight-current-time" : ""}>
                                                    <IonRow>
                                                        <IonCol size="9">
                                                            <IonCardTitle >
                                                                <span>{result.location_name}</span>
                                                                <span className="start-time-font-size">{`出發時間：${result.start_time.slice(0, 5)}`}</span>
                                                            </IonCardTitle>
                                                            <IonCardContent>
                                                                <div>Address：{result.address}</div>
                                                                <hr></hr>
                                                                <div>Content：{result.note}</div>
                                                            </IonCardContent>
                                                        </IonCol>
                                                        <IonCol size="3" className="journey-gps-path-container">
                                                            <div className="journey-gps-path">
                                                               {on&&  <div><IonButton color="danger" onClick={async (event) => {
                                                                    event.preventDefault();
                                                                    await dispatch(delJourney(result.id))
                                                                    await dispatch(getExactlyTrip(trip_id));
                                                                    await dispatch(getJourney(trip_id))
                                                                }}><img width="20" src={journeyDel}/></IonButton></div>}
                                                                <IonButton type="submit" color="none">
                                                                    {/* <IonImg src={journeyGps}></IonImg> */}
                                                                    {today[0] === dateResult.allDate && currentHour as any == result.start_time.slice(0, 2) ? <IonImg src={journeyGpsWhite} alt="journeygpswhite"></IonImg> : <IonImg src={journeyGps} alt="journeygps"></IonImg>}
                                                                </IonButton>

                                                            </div>
                                                        </IonCol>
                                                    </IonRow>
                                                </IonCard>

                                            </form>
                                        </div>
                                    </IonCol>
                                ))}
                            </IonRow>
                        </IonGrid>
                        <Link to={`/createjourney?user_id=${user_id}&email=${email}&trip_id=${trip_id}&journey_date=${dateResult.allDate}`}><div className="add-journey-button-container"><div className="add-journey-title">Create New Journey</div></div></Link>
                    </div>

                ))}


            </IonContent>


            <IonFooter>
                <div className="message-board-container">
                    <Link to={`/comment?user_id=${user_id}&email=${email}&trip_id=${trip_id}`}><IonButton>Message Board</IonButton></Link>
                </div>
            </IonFooter>

        </IonApp>
    )
}