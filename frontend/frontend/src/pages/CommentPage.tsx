import React, { useState, useEffect } from "react";
import { RootState } from "../store";
import { useSelector, useDispatch } from "react-redux";
import { IonApp, IonMenu, IonHeader, IonToolbar, IonContent, IonList, IonItem, IonRouterLink, IonLabel, IonTitle, IonButton, IonMenuButton, IonInput, IonTextarea, IonFooter, IonImg, IonToast } from "@ionic/react";
import { Menu } from "./HomePage";
import "../css/Comment.scss";
import send from "../icon/send.svg";
import { postComment, getComment, postCommentData, CommentFailed } from "../redux/comment/actions";
import useReactRouter from 'use-react-router';
import { Link } from "react-router-dom";
import back from "../icon/back.svg";

export function CommentPage() {
    const comments = useSelector((state: RootState) => state.comment.allComment)

    const dispatch = useDispatch();
    const [text, setText] = useState("");

    const { location } = useReactRouter();
    const tripId = location.search
    const params = new URLSearchParams(tripId)
    const trip_id = params.get('trip_id')
    const email = params.get('email')
    const user_id = params.get('user_id')

    const errorMessage = useSelector((state: RootState) => state.comment.error);
    const [errorToast, setErrorToast] = useState(false);
    const [errorMsg, setErrorMsg] = useState();

    useEffect(() => {
        if (errorMessage) {
            setErrorToast(true)
           
        }
    }, [errorMessage])

    useEffect(() => {
        dispatch(getComment(trip_id))
        dispatch(postCommentData(text, email as any))
    }, [])

    return (
        <IonApp>
            <Menu />

            <IonHeader id="main-content">
                <IonToolbar mode="ios" color="primary" className="comment-toolbar-button-container">
                    <IonTitle className="toolbar-title">Message Board</IonTitle>
                    <Link to={`/journey?trip_id=${trip_id}&email=${email}&user_id=${user_id}`} slot="start">
                        <IonButton><IonImg src={back}></IonImg></IonButton>
                    </Link>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                {comments.map((data) => (
                    <IonItem className="show-comment-container">
                        <IonLabel>
                            <h2>{data.email}</h2>
                            <p>{data.text}</p>
                        </IonLabel>
                    </IonItem>
                ))}
            </IonContent>

            <IonFooter>
                <form onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(postComment(text, trip_id, email as any));
                  
                    setText("")
                }}>
                    <IonItem className="input-comment-container" mode="ios">
                        <IonTextarea placeholder="Messages" name="text" value={text} onIonChange={(e) => setText(e.detail.value!)}></IonTextarea>
                        <IonButton
                            type="submit"
                            onClick={async () => {
                                setErrorToast(false)
                                await dispatch(CommentFailed(""))
                            }}><IonImg src={send}></IonImg></IonButton>
                    </IonItem>
                    <IonToast
                        mode="ios"
                        isOpen={errorToast}
                        onWillDismiss={() => setErrorToast(false)}
                        message={errorMsg}
                        duration={1200}
                        color="danger"
                    />
                </form>
            </IonFooter>


        </IonApp>
    )
}