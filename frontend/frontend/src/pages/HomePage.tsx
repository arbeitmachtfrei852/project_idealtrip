import React, { useState, useEffect } from "react";
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonFab,
  IonIcon,
  IonModal,
  IonButton,
  IonMenuButton,
  IonMenu,
  IonContent,
  IonItem,
  IonList,
  IonLabel,
  IonApp,
  IonInput,
  IonDatetime,
  IonCard,
  IonActionSheet,
  IonCardTitle,
  IonCardSubtitle,
  IonCol,
  IonRow,
  IonGrid,
  IonRouterLink,
  IonToast,
} from "@ionic/react";
import { create, trash, people, ellipsisVertical } from "ionicons/icons";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../store";
import { logout } from "../redux/auth/actions";

import "../css/Home.scss";
// /* Core  CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";
// /* Basic CSS for apps built with Ionic */
import "@ionic/react/css/typography.css";
// /* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import { getTrip, addTrip, delTripData, addFriendToTrip, addTripFailed, loadAddedEmail } from "../redux/trip/actions";
import moment from 'moment';
import { getExactlyTrip } from "../redux/journey/actions";
import useReactRouter from 'use-react-router';


export function Menu() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const dispatch = useDispatch();

  const { location } = useReactRouter();
  const param = location.search;
  const params = new URLSearchParams(param);
  const email = params.get('email');

  return (
    <IonMenu side="start" menuId="first" contentId="main-content">
      <IonHeader>
        <IonToolbar color="light">
          {isAuthenticated ? (
            <div className="display-current-user-email">{email}</div>
          ) : (
              <div className="display-current-user-email">Please Login</div>
            )}
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>
            <IonRouterLink href="/" onClick={async () => { await dispatch(logout()); }}><IonLabel>Logout</IonLabel></IonRouterLink>
          </IonItem>
        </IonList>
      </IonContent>
    </IonMenu>
  )
}

export function HomePage() {
  const [showModal, setShowModal] = useState(false);
  const [showActionSheetTripId, setShowActionSheetTripId] = useState<number | undefined>(undefined);
  const [showModalTripId, setShowModalTripId] = useState<number | undefined>(undefined);

  const dispatch = useDispatch();
  const user: any = useSelector((state: RootState) => state.auth.user);
  const trips = useSelector((state: RootState) => state.trip.trips);
  const listEmail = useSelector((state: RootState) => state.trip.email);
  const errorMessage = useSelector((state: RootState) => state.trip.error);

  const [errorToast, setErrorToast] = useState(false);
  const [errorMsg, setErrorMsg] = useState<undefined | string>("");

  const [tripName, setTripName] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [email, setEmail] = useState("");
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);

  useEffect(() => {
    if (errorMessage) {
      setErrorToast(true)
      setErrorMsg(errorMessage)
    }
  }, [errorMessage])

  useEffect(() => {
    dispatch(getTrip());
  }, [dispatch]);

  return (
    <div>
      <IonApp>

        <Menu />
        
        <div id="main-content" className="main-content">
          <IonHeader>
            <IonToolbar mode="ios" color="primary">
              <IonTitle className="toolbar-title">Trips</IonTitle>
              <IonButton type="button" slot="start">
                <IonMenuButton color="light"></IonMenuButton>
              </IonButton>
            </IonToolbar>
          </IonHeader>
        </div>

        <IonContent>
          {!isAuthenticated && <p>Please Log in</p>}
          <IonActionSheet
            mode="ios"
            isOpen={showActionSheetTripId !== undefined}
            onWillDismiss={() => setShowActionSheetTripId(undefined)}
            cssClass="my-custom-class"
            buttons={[
              {
                text: "Invite friends",
                icon: people,
                handler: async () => {
                  await setShowModalTripId(showActionSheetTripId);
                  await dispatch(loadAddedEmail(showActionSheetTripId));

                  console.log(showActionSheetTripId)
                },
              },
              // {
              //   text: "Edit Journey",
              //   icon: create,
              // },
              {
                text: "Delete Journey",
                icon: trash,
                role: "destructive",
                handler: async () => {
                  await dispatch(delTripData(showActionSheetTripId));
                },
              },
              { text: "cancel", role: "cancel" },
            ]}
          ></IonActionSheet>

          <div className="count-trip-container">
            {isAuthenticated && trips.length + " in total"}
          </div>

          <IonGrid>
            <IonRow>
              {trips.map((trip) => (
                <IonCol sizeXs="12" sizeMd="6" sizeLg="3">
                  <IonCard mode="ios">
                    <div className="card-button-container">
                      <IonButton
                        mode="ios"
                        className="card-more-button"
                        onClick={async () => {
                          await setShowActionSheetTripId(trip.trip_id);
                        }}
                        expand="block"
                      >
                        <IonIcon icon={ellipsisVertical} />
                      </IonButton>
                    </div>
                    <Link to={`/journey?user_id=${user.id}&email=${user.email}&trip_id=${trip.trip_id}`} onClick={() => {   // step1:由trip.trip_id pass個tripID 去 
                      // dispatch(getGroupMaterials(trip.trip_id))
                      // dispatch(getComment(trip.trip_id))
                      dispatch(getExactlyTrip(trip.trip_id))
                    }}>
                      <div className="card-content-container">
                        <IonCardTitle>{trip.trip_name}</IonCardTitle>
                        <IonCardSubtitle>
                          Departure Date：{moment(trip.start_date).format('YYYY-MM-DD')}
                        </IonCardSubtitle>
                        <IonCardSubtitle>
                          Return Date：{moment(trip.end_date).format('YYYY-MM-DD')}
                        </IonCardSubtitle>
                      </div>
                    </Link>
                  </IonCard>
                </IonCol>
              ))}
            </IonRow>
          </IonGrid>
        </IonContent>


        {/* 新增旅伴 */}
        <IonModal isOpen={showModalTripId !== undefined} cssClass="my-custom-class" onWillDismiss={() => setShowModalTripId(undefined)}>
          <IonContent scrollEvents={true}>
            <form
              onSubmit={async (event) => {
                event.preventDefault();
                setShowModalTripId(showActionSheetTripId);

                await dispatch(
                  addFriendToTrip(email, showModalTripId)
                );

              }}
            >
              <IonToolbar color="primary" mode="ios">
                <IonTitle>Invite friends</IonTitle>
                <IonButton slot="start"
                  onClick={() => {
                    setShowModalTripId(undefined);
                    setEmail("");
                  }}>BACK</IonButton>
                <IonButton type="submit" slot="end"
                  onClick={async () => {
                    // setShowModalTripId(showActionSheetTripId);
                    setEmail("");
                    await dispatch(addTripFailed(""))
                  }}>ADD</IonButton>
              </IonToolbar>
              <IonList>
                <IonItem>
                  <IonLabel position="floating" color="primary">Enter e-mail</IonLabel>
                  <IonInput
                    color="primary"
                    required
                    type="email"
                    name="email"
                    value={email}
                    onIonChange={(e) => setEmail(e.detail.value!)}
                  />
                </IonItem>
              </IonList>
              <IonList mode="md">
                <div className="list-email-container">
                  <div className="list-email-title">Trip members</div>
                  {listEmail.map((list) => (
                    <div>
                      <hr></hr>
                      <div className="list-email">{list.email}</div>
                      <hr></hr>
                    </div>
                  ))}
                </div>
              </IonList>
              <IonToast
                mode="ios"
                isOpen={errorToast}
                onDidDismiss={() => setErrorToast(false)}
                message={errorMsg}
                duration={1200}
                color="danger"
              />
            </form>
          </IonContent>
        </IonModal>
        {/* End 新增旅伴 */}

        <IonFab vertical="bottom" slot="fixed" color="primary">
          <IonModal
            isOpen={showModal}
            cssClass="my-custom-class"
            onDidDismiss={() => setShowModal(false)}
          >
            <IonContent scrollEvents={true}>
              <form
                onSubmit={async (event) => {
                  event.preventDefault();
                  let result = await dispatch(addTrip(tripName, startDate, endDate));
                  if (!result) {
                    setShowModal(false)
                  }
                }}
              >
                <IonToolbar color="primary" mode="ios">
                  <IonTitle>Create Trip</IonTitle>
                  <IonButton slot="start" onClick={() => {
                    setShowModal(false)
                  }}>
                    BACK
                </IonButton>
                  <IonButton
                    type="submit"
                    expand="block"
                    onClick={async () => {
                      await dispatch(addTripFailed(""))
                      setTripName("");
                      setStartDate("");
                      setEndDate("");
                    }}
                    slot="end"
                  >
                    ADD
                </IonButton>
                </IonToolbar>
                <IonList>
                  <IonItem>
                    <IonLabel position="floating">Name of Trip</IonLabel>
                    <IonInput
                      type="text"
                      name="tripName"
                      value={tripName}
                      onIonChange={(e) => setTripName(e.detail.value!)}
                    ></IonInput>
                  </IonItem>
                  <IonItem>
                    <IonLabel>Departure Date</IonLabel>
                    <IonDatetime
                      displayFormat="YYYY/MM/DD"
                      displayTimezone="utc"
                      min="2020"
                      max="2099"
                      placeholder="Select Date"
                      name="startDate"
                      value={startDate}
                      onIonChange={(e) => setStartDate(e.detail.value!)}
                    ></IonDatetime>
                  </IonItem>
                  <IonItem>
                    <IonLabel>Return Date</IonLabel>
                    <IonDatetime
                      displayFormat="YYYY/MM/DD"
                      displayTimezone="utc"
                      min="2020"
                      max="2099"
                      placeholder="Select Date"
                      name="endDate"
                      value={endDate}
                      onIonChange={(e) => setEndDate(e.detail.value!)}
                    ></IonDatetime>
                  </IonItem>
                </IonList>
                <IonToast
                  mode="ios"
                  isOpen={errorToast}
                  onDidDismiss={() => setErrorToast(false)}
                  message={errorMsg}
                  duration={1200}
                  color="danger"
                />
              </form>
            </IonContent>
          </IonModal>

          <div className="create-trip-container">
            <IonButton
              className="create-trip-button"
              shape="round"
              onClick={() => setShowModal(true)}
            >
              <IonLabel>CREATE</IonLabel>
            </IonButton>
          </div>
        </IonFab>
      </IonApp>
    </div>
  );
}
