import React, { useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../store";
import { IonApp, IonHeader, IonToolbar, IonTitle, IonButton, IonMenuButton, IonContent } from "@ionic/react";
import { Menu } from "./HomePage";

export function MaterialPage() {

  const groupMaterial = useSelector((state: RootState) => state.groupMaterial.groups)
  
  return (
    <IonApp>
      <Menu />

      <IonHeader id="main-content">
        <IonToolbar mode="ios" color="primary">
          <IonTitle className="toolbar-title">Message Board</IonTitle>
          <IonButton type="button" slot="start">
            <IonMenuButton color="light"></IonMenuButton>
          </IonButton>
        </IonToolbar>
      </IonHeader>


      <IonContent>
        <div>
          {groupMaterial.map(data => (
            <div>{data.item}</div>
          ))}
        </div>
      </IonContent>

    </IonApp>
  )
}
