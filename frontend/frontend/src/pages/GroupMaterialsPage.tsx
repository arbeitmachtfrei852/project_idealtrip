import React, { useState, useEffect } from "react"
import { connect, useDispatch, useSelector } from "react-redux"
import { addGroupMaterials, loadGroupMaterials, Group, postGroupMaterials, getGroupMaterials, deleteGroupMaterials } from '../redux/gpMaterial/actions'
import { RootState } from '../store';
import { useFormState } from "react-use-form-state";
import Form from "react-bootstrap/Form";
import Button from 'react-bootstrap/Button';
import { IonInput, IonButton, IonApp, IonHeader, IonTitle, IonToolbar, IonMenuButton, IonImg, IonContent, IonItem, IonCheckbox, IonFooter, IonLabel, IonList } from '@ionic/react'
import useReactRouter from 'use-react-router';
import "../css/Materials.scss";
import { Menu } from "./HomePage";
import back from "../icon/back.svg";
import { Link } from "react-router-dom";
import del from "../icon/del.svg";

export function GroupMaterials() {

    const dispatch = useDispatch()

    const groupItem = useSelector((state: RootState) => state.groupMaterial.groups)

    const [item, setItem] = useState("");
    const [assignee, setAssignee] = useState<string>("");
    // const [checked, setChecked] = useState<boolean>(false)

    const { location } = useReactRouter();
    const tripId = location.search;
    const params = new URLSearchParams(tripId);
    const trip_id = params.get('trip_id');
    const email = params.get('email')
    const user_id = params.get('user_id')

    useEffect(() => {
        dispatch(getGroupMaterials(trip_id))
    }, [])

    return (
        <IonApp>
            <Menu />
            <IonHeader>
                <IonToolbar mode="ios" color="primary" className="material-toolbar-button-container">
                    <IonTitle className="toolbar-title">Group items</IonTitle>
                    <Link to={`/journey/?trip_id=${trip_id}&user_id=${user_id}&email=${email}`} slot="start">
                        <IonButton><IonImg src={back}></IonImg></IonButton>
                    </Link>
                </IonToolbar>
            </IonHeader>



            <IonContent>
                <Link to={`/groupMaterials?trip_id=${trip_id}&user_id=${user_id}&email=${email}`}><IonButton class="itembutton" mode="ios">Group</IonButton></Link>
                <Link to={`/individualMaterials?trip_id=${trip_id}&user_id=${user_id}&email=${email}`}><IonButton class="itembutton" mode="ios">Individual</IonButton></Link>


                <form onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(postGroupMaterials(trip_id, item, assignee))
                    setItem("");
                    setAssignee("")
                }}>
                    <div className="add-material-item-container">
                        <div className="add-material-input">
                            <IonInput type="text" placeholder=" Item" value={item} onIonChange={(e) => setItem(e.detail.value!)}></IonInput>
                        </div>
                        <div className="add-material-input">
                            <IonInput type="text" placeholder=" By whom" value={assignee} onIonChange={(e) => setAssignee(e.detail.value!)}></IonInput>
                        </div>
                        <IonButton type="submit">ADD</IonButton>
                    </div>
                    <hr></hr>
                </form>

                <IonTitle>Group Item List</IonTitle>
                <br/>
                {groupItem.map(data => (
                    <IonList mode="ios">
                        <div className="material-data-container">

                            <div className="material-checkbox-item-delete-container">
                                <div className="material-checkbox-item">
                                    {/* <IonCheckbox /> */}
                                    <div className="material-item">{data.item}</div>
                                </div>
                                <div className="remove-button"><IonButton onClick={async(event)=>{
                                    event.preventDefault();
                                    await dispatch(deleteGroupMaterials(data.id))
                                    console.log(data)
                                }} color="#000000"><IonImg src={del} /></IonButton></div>
                            </div>
                            <div className="group-material-assignee">By - {data.assignee}</div>
                            <hr></hr>
                        </div>
                    </IonList>
                ))}
            </IonContent>

            <IonFooter>
                {/* <div className="save-group-material-button">
                    <IonButton mode="ios">Save</IonButton>
                </div> */}
            </IonFooter>

        </IonApp >
    )
}


// class GroupMaterialsComponent extends React.Component<{items:Group[],addGroupMaterials:typeof addGroupMaterials}>{



//     render(){
//         return(
//             <div>

//                 <form onSubmit={}>
//                     <input value={this.props.input} onChange={this.props.submit}/>
//                     <button type="button" onClick={this.props.addGroupMaterials}>ADD</button>
//                 </form>
//                 <p>{this.props.items}</p>
//                   {this.props.items.map(data=>({data.item}))}
//             </div>
//         )
//     } 
// }

// const mapStateToProps = (state:RootState)=>{
//     return {items: state.groupMaterial.groups}
// }

// const mapDispatchToProps = {
// addGroupMaterials
// }

// export default connect(mapStateToProps,mapDispatchToProps)(GroupMaterialsComponent)