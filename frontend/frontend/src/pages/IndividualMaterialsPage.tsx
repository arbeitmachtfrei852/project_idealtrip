
import { useDispatch, useSelector } from "react-redux"
import { IonInput, IonApp, IonHeader, IonToolbar, IonTitle, IonButton, IonImg, IonContent, IonFooter, IonItem, IonCheckbox, IonList } from "@ionic/react"
import { useFormState } from "react-use-form-state"
import { RootState } from "../store"
import React, { useState, useEffect } from "react"
import {postIndMaterials, getIndMaterial, deleteMaterials, checkedMaterials, updateMaterials} from '../redux/indMaterial/actions'
import useReactRouter from 'use-react-router';
import { Menu } from "./HomePage"
import { Link } from "react-router-dom"
import back from "../icon/back.svg";
import InputGroup from "react-bootstrap/InputGroup";
import del from "../icon/del.svg";

export function IndividualMaterials() {


    const dispatch = useDispatch()

 
    const indItem = useSelector((state: RootState) => state.indMaterial.individual)
    const errorMessage = useSelector((state: RootState) => state.indMaterial.error);

    const [item, setItem] = useState('')
    const [itemID, setItemID] = useState<number|undefined>(undefined)


    const [errorToast, setErrorToast] = useState(false);
    const [errorMsg, setErrorMsg] = useState<undefined | string>("");

    const { location } = useReactRouter();
    const tripId = location.search;
    const params = new URLSearchParams(tripId);
    const trip_id = params.get('trip_id');
    const email = params.get('email')
    const user_id = params.get('user_id')

    useEffect(() => {
        if (errorMessage) {
          setErrorToast(true)
          setErrorMsg(errorMessage)
        }
      }, [errorMessage])
    
      useEffect(() => {
        dispatch(getIndMaterial(trip_id));
      }, [dispatch]);



    return (
        <IonApp>
            <Menu />
            <IonHeader>
                <IonToolbar mode="ios" color="primary" className="material-toolbar-button-container">
                    <IonTitle className="toolbar-title">Individual Item List</IonTitle>
                    <Link to={`/journey/?trip_id=${trip_id}&user_id=${user_id}&email=${email}`} slot="start">
                        <IonButton><IonImg src={back}></IonImg></IonButton>
                    </Link>
                </IonToolbar>
            </IonHeader>


            <IonContent>
                <Link to={`/groupMaterials?trip_id=${trip_id}&user_id=${user_id}&email=${email}`}><IonButton class="itembutton"  mode="ios">Group</IonButton></Link>
                <Link to={`/individualMaterials?trip_id=${trip_id}&user_id=${user_id}&email=${email}`}><IonButton class="itembutton"  mode="ios">Individual</IonButton></Link>
                <form onSubmit={async (event) => {
                    event.preventDefault();
                    await dispatch(postIndMaterials(trip_id, item))
                    setItem("")
                }}>
                    <div className="add-material-item-container">
                        <div className="add-material-input">
                            <IonInput type="text" placeholder=" Item" value={item} onIonChange={(e) => setItem(e.detail.value!)}></IonInput>
                        </div>
                        <IonButton type="submit">ADD</IonButton>
                    </div>
                    <hr></hr>
                </form>
                <IonTitle>My Items</IonTitle>
                <br/>
                {indItem.map(data => (
                    <IonList mode="ios">
                        <div className="material-data-container">
                            <div className="material-checkbox-item-delete-container">
                                <div className="material-checkbox-item">
                                    {/* <IonCheckbox onIonChange={async(event)=>{
                                        event.preventDefault();
                                        await dispatch(checkedMaterials(data.id))
                                        console.log(data)
                                    }}/> */}
                                    <div className="material-item">{data.item}</div>
                                </div>
                                <div className="remove-button"><IonButton onClick={
                                    async(event) =>{
                                        event.preventDefault();
                                        await dispatch(deleteMaterials(data.id))
                                        console.log(data)
                                    }
                                } color="#000000"><IonImg src={del} /></IonButton></div>
                            </div>
                            <hr></hr>
                        </div>
                    </IonList>
                ))}
            </IonContent>

            <IonFooter>
                {/* <div className="save-group-material-button">
                    <IonButton mode="ios">Save</IonButton>
                </div> */}
            </IonFooter>
        </IonApp>
    )
}