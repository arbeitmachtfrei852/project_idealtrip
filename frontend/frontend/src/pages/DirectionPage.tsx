import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import GoogleMapReact, { ChildComponentProps } from "google-map-react";
import { RootState } from "../store";
import { Menu } from "./HomePage";
import "../css/Direction.scss";
// Icon
import locat from "../icon/location.svg";
import map from "../icon/map.svg";
import here from "../icon/here.svg";
import marker from "../icon/mypos.png";
import arrow from "../icon/arrow.svg";
import walking from "../icon/walking.svg";
import bus1 from "../icon/bus.svg";
import train1 from "../icon/train.svg";
import time from "../icon/time.svg";
import { walk, train, bus } from "ionicons/icons";
import { IonHeader, IonToolbar, IonTitle, IonIcon, IonButton, IonMenuButton, IonMenu, IonContent, IonItem, IonList, IonApp, IonImg, IonGrid, IonRow, IonCol, } from "@ionic/react";
import useReactRouter from 'use-react-router';
import back from "../icon/back.svg";
import { loadExactlyJourney } from "../redux/map/actions";

const API_KEY = `${process.env.REACT_APP_API_KEY_GOOGLE_MAPS}`;

let routeSteps: any[] = [];
let mySteps: any[] = [];
let routeInfos: any[] = [];
let info: any[] = []

export function MyPos(props: ChildComponentProps) {
  return (
    <div className="you-are-here">
      <img src={here} width="30px" />
      <div>您在此處</div>
    </div>
  );
}

export function DirectionInfo() {
  info = routeInfos.map((info, i) => (
    <div>
      <div className="route-info-container">
        <img src={time} />
        <div className="info-duration">需時約{info.duration.split(" ")}</div>
        <span>{info.distance.split(" ")}</span>
      </div>

      <div className="start-end-point-info-container">
        <img src={map} />
        <span>目的地：{info.end_address}</span>
      </div>
    </div>
  ));

  return <div>{info}</div>;
}

export function MyRoute() {
  mySteps = [];

  for (let routeStep of routeSteps) {
    let { distance, duration, instructions, transit, steps } = routeStep;
    if (routeStep.transit) {
      mySteps.push({ distance, duration, instructions, transit });
    } else {
      mySteps.push({ distance, duration, instructions, steps });
    }
  }

  let step = mySteps.map((info) => {
    let transit_instruction: string = info.instructions
      .replace("捷運", "地鐵")
      .split(" ");

    if (!info.transit) {
      let walk_instruction: string = info.instructions.replace(
        /(<([^>]+)>)/gi,
        ""
      );

      return (
        <div>
          <div className="walk-detail">

            <IonRow>
              <IonCol size="1">
                <img src={walking} />
              </IonCol>
              <IonCol size="11">
                <div className="instruction">{walk_instruction}</div>
              </IonCol>
            </IonRow>

            {/* <div className="instruction-container">
              <img src={walking} />
              <div className="instruction">{walk_instruction}</div>
            </div> */}
            <IonRow>
              <IonCol size="12">
                <span>
                  {info.distance.text} | {" 約 "}
                  {info.duration.text}
                </span>
              </IonCol>
            </IonRow>
            <div className="my-hr"></div>
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="transit-detail">

          <IonRow>
            <IonCol size="1">
              {info.transit.line.short_name ? <img src={bus1} /> : <img src={train1} />}
            </IonCol>
            <IonCol size="11">
              <div className="transit-line-name-container">
                {info.transit.line.short_name || info.transit.line.name}
              </div>
      
            </IonCol>
            <IonCol>

            <div className="instruction">{transit_instruction}方向</div>
            </IonCol>
          </IonRow>

          <div>
            <span>
              {info.distance.text} | {" 約 "} {info.duration.text}
            </span>
            <div className="transit-departure-arrival-container">
              <div>{info.transit.departure_stop.name}</div>
              <div className="transit-number-stop-container">
                <img src={arrow} />({info.transit.num_stops}個站)
              </div>
              <div>{info.transit.arrival_stop.name}</div>
            </div>
            <div className="my-hr"></div>
          </div>
        </div>
      </div>
    );
  });
  return <div>{step}</div>;
}

export function Marker({ lat, lng }: any) {
  return (
    <img src={marker} width="30px" />
  )
}

export function DirectionPage() {

  // const Marker = ({ }: any) => <img src={marker} width="30px" />

  // const allJourney = useSelector((state: RootState) => state.journey.journey);

  const [map, setMap] = useState<any>(null);
  const [maps, setMaps] = useState<any>(null);

  const [routeSegment, setRouteSegment] = useState(0);

  const { location } = useReactRouter();
  const latLng = location.search
  const params = new URLSearchParams(latLng)
  const latPoint = params.get('lat')
  const lngPoint = params.get('lng')
  const trip_id = params.get('trip_id')
  const journey_item_id = params.get('journey_item_id')
  const email = params.get('email')

  const dispatch = useDispatch();

  const [start, setStart] = useState({ lat: 0, lng: 0 });
  const [end, setEnd]: any = useState({ lat: 0, lng: 0 });

  const [zoom, setZoom] = useState(17);
  const [transit, setTransit] = useState(["null"]);
  const [travelMode, setTravelMode] = useState("");

  const latLngStore = useSelector((state: RootState) => state.map.journey)



  function makeRequest() {
    // setEnd({lat:latPoint,lng:lngPoint)})    
    let xhr = new XMLHttpRequest();
    xhr.open(
      "POST",
      `https://www.googleapis.com/geolocation/v1/geolocate?key=${process.env.REACT_APP_API_KEY_GOOGLE_MAPS}`
    );
    xhr.onload = async function () {
      var response = JSON.parse(this.responseText);
      await setStart({
        lat: response.location.lat,
        lng: response.location.lng,
      });
      console.log("request geo");
    };
    xhr.send();
  }


  useEffect(() => {
    if (map == null || maps == null) {

      return;
    }
    routeSteps = []
    routeInfos = []

    makeRequest()
    for (let result of latLngStore) {
      setEnd({ lat: result.lat, lng: result.lng })
    }

    // DirectionInfo()
    // MyRoute()
    var directionsService = new maps.DirectionsService();
    var directionsRenderer = new maps.DirectionsRenderer();

    directionsRenderer.setMap(map);
    directionsService.route(
      {
        origin: new maps.LatLng(start.lat, start.lng),
        destination: new maps.LatLng(end.lat, end.lng),
        travelMode: travelMode,
        transitOptions: {
          departureTime: new Date(),
          modes: transit,
          routingPreference: "FEWER_TRANSFERS",
        },
        unitSystem: maps.UnitSystem.METRIC,
      },
      function (response: any, status: any) {
        if (status == "OK") {
          directionsRenderer.setDirections(response);
          var route = response.routes[0];
          var info = route.legs;
          routeSteps = route.legs[0].steps;
          console.log(routeSteps)
          routeInfos = [];
          for (let i of info) {
            routeInfos.push({
              distance: i.distance.text,
              duration: i.duration.text,
              start_address: i.start_address,
              end_address: i.end_address,
            });
          }

          for (var i = 0; i < route.legs.length; i++) {
            setRouteSegment(i + 1);
          }
          setRouteSegment(0);
        } else {
          window.alert("No transportation suggestions, please chose other means of transportation");
        }
      }
    );


    return () => {
      directionsRenderer.setMap(null);
    };
  }, [map, maps, transit,]);

  return (
    <IonApp>

      <Menu />
      <IonHeader>
        <IonToolbar mode="ios" color="primary" className="direction-toolbar-button-container">
          <IonTitle className="toolbar-title">Routes</IonTitle>
          <Link to={`/journey?trip_id=${trip_id}&email=${email}`} slot="start">
            <IonButton><IonImg src={back}></IonImg></IonButton>
          </Link>
        </IonToolbar>
      </IonHeader>


      <IonContent>
        <div className="map">
          <div className="you-are-here-button">
            <button
              onClick={() => {
                setEnd(start);
                setZoom(16);
                makeRequest();
              }}
            >
              <img src={locat} width="18px" />
            </button>
          </div>

          <GoogleMapReact
            bootstrapURLKeys={{ key: API_KEY }}
            center={end}
            zoom={zoom}
            distanceToMouse={() => {
              return 0;
            }}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => {
              setMap(map);
              setMaps(maps);
            }}
          >

            <Marker lat={end.lat} lng={end.lng} />
            <MyPos lat={start.lat} lng={start.lng} />

          </GoogleMapReact>
        </div>

        <div className="travel-mode-button">
          <Button
            onClick={() => {
              { latLngStore.map(result => setEnd({ lat: result.lat, lng: result.lng })) }
              setTravelMode("WALKING");
              setTransit([]);
              makeRequest()
            }}
          >
            <IonIcon src={walk} />
            <div>Walking</div>
          </Button>

          <Button
            onClick={() => {
              { latLngStore.map(result => setEnd({ lat: result.lat, lng: result.lng })) }
              setTravelMode("TRANSIT");
              setTransit(["BUS"]);
              makeRequest()
            }}
          >
            <IonIcon src={bus} />
            <div>Bus</div>
          </Button>
          <Button
            onClick={() => {
              { latLngStore.map(result => setEnd({ lat: result.lat, lng: result.lng })) }
              setTravelMode("TRANSIT");
              setTransit(["RAIL"]);
              makeRequest()
            }}
          >
            <IonIcon src={train} />
            <div>Rail</div>
          </Button>
        </div>
        <DirectionInfo />
        <MyRoute />
      </IonContent>
    </IonApp>
  );
}








{/* {pos?.map((pos, i) => (
          <Button
            onClick={() => {
              setEnd({ lat: pos.lat, lng: pos.lng });
              setZoom(16);
              setTravelMode("TRANSIT");
              setTransit([]);
            }}
            key={i}
            className={
              Math.abs(end.lat - pos.lat) < 0.0001 &&
                Math.abs(end.lng - pos.lng) < 0.0001
                ? "active"
                : ""
            }
          >
            pos{i}
          </Button>
        ))} */}