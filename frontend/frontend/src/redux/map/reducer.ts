import { Journey, MapActions } from "./actions"

export type MapState = {
    journey: Journey[];
}

const initialState: MapState = {
    journey: [],
}

export function mapReducer(state: MapState = initialState, action: MapActions): MapState {
   switch(action.type){
       case '@@map/LOADED_EXACTLY_JOURNEY':
           return{
                ...state,
                journey: action.exactlyJourney.map(data=>data)
           }
       default:
           return state;
   }
  
}
