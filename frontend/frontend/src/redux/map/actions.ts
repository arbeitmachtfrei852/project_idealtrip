import { ThunkDispatch, RootState } from "../../store";
import { push } from "connected-react-router";

export interface Journey {
    lat: number,
    lng: number
}

export function loadExactlyJourneyData(exactlyJourney: Journey[]) {
    return {
        type: "@@map/LOADED_EXACTLY_JOURNEY" as "@@map/LOADED_EXACTLY_JOURNEY",
        exactlyJourney
    };
}

export type MapActions = ReturnType<typeof loadExactlyJourneyData>

export function loadExactlyJourney(tripId:number, journeyItemId:number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/journey/${tripId}/${journeyItemId}`, {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`
                }
            });

            const json = await res.json();
            dispatch(loadExactlyJourneyData(json))

        } catch (e) {
            console.error(e);
        }
    }
}