import { Comment, CommentActions } from "./actions"


export interface CommentState{
    allComment: Comment[];
    tripId: string | null
    error: string |null
}

const initialState: CommentState = {
    allComment: [],
    tripId: "",
    error:null
}

export function commentReducer(state: CommentState = initialState, action: CommentActions): CommentState {
    switch(action.type){
        case '@@comment/LOADED_COMMENT':
            return{
                ...state,
                allComment: action.allComment.map(data=>data),
                tripId: action.tripId
            }
        case '@@comment/POSTED_COMMENT':
            return{
                ...state,
                allComment:[
                    {
                        text:action.text,
                        email: action.email
                    },
                    ...state.allComment
                ]
            }
        case '@@comment/COMMENT_FAILED':
            return{
                ...state,
                error: action.error
            }
        default:
            return state
    }
}