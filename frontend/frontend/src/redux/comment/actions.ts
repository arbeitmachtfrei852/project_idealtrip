import { ThunkDispatch, RootState } from "../../store"

export interface Comment {
  text: string
  email: string
}


export function loadCommentData(allComment: Comment[], tripId: string | null) {
  return {
    type: "@@comment/LOADED_COMMENT" as "@@comment/LOADED_COMMENT",
    allComment,
    tripId
  }
}

export function postCommentData(text: string, email: string) {
  return {
    type: "@@comment/POSTED_COMMENT" as "@@comment/POSTED_COMMENT",
    text,
    email
  }
}

export function CommentFailed(error: string) {
  return {
    type: "@@comment/COMMENT_FAILED" as "@@comment/COMMENT_FAILED",
    error
  }
}

export type CommentActions = ReturnType<typeof loadCommentData | typeof postCommentData | typeof CommentFailed>


export function getComment(trip_id: string | null) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/trip/${trip_id}/comment`, {
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      });

      const json = await res.json();
      dispatch(loadCommentData(json, trip_id))

    } catch (e) {
      console.error(e);
    }
  }
}

export function postComment(text: string, trip_id: string | null , email:string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/trip/${trip_id}/post/comment`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: JSON.stringify({ text }),
      });

      const json = await res.json();

      if (res.status != 200) {
        return dispatch(CommentFailed(json.error))
      }

     dispatch(postCommentData(text,email))

    } catch (e) {
      console.error(e);
    }
  };
}