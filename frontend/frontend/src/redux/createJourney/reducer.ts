import { CreateJourneyActions, Journey } from './actions'

export interface CreateJourneyState {
    journeyInfo: Journey[]
}

const initCreateState: CreateJourneyState = {
    journeyInfo: []
}


export function createJourneyReducer(state: CreateJourneyState = initCreateState, action: CreateJourneyActions): CreateJourneyState {
    switch (action.type) {
        case "ADD@createJourney":
            return {
                ...state,
                journeyInfo: [{
                    id: action.id,
                    location_name: action.location_name,
                    address: action.address,
                    day: action.day,
                    trip_id: action.trip_id,
                    lat: action.lat,
                    lng: action.lng,
                    note: action.note,
                    start_time: action.start_time
                },
                ...state.journeyInfo]
            }
        default:
            return state
    }
}

