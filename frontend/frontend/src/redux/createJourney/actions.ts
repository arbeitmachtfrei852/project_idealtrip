import { ThunkDispatch, RootState } from "../../store"

export interface Journey{
    id:number,
    location_name:string,
    address: string,
    day:Date,
    trip_id:number,
    lat:number,
    lng:number,
    note:string,
    start_time:string
}


export function addJourney(id:number,location_name:string, trip_id:number,
    day:Date,address:string, lat:number, lng:number, note:string,start_time:string,){
    return {
        type:"ADD@createJourney" as "ADD@createJourney",
        id,
        day,
        location_name,
        address,   
        lat,
        lng,
        note,
        trip_id,
        start_time}
}


export type CreateJourneyActions = ReturnType<typeof addJourney>

export function postJourney(
    trip_id:string|null, day:Date, location_name:string,address:string,lat:number,
    lng:number, note:string, start_time:string){
    return async(dispatch: ThunkDispatch, getState:()=> RootState)=>{
        try{
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/createjourney/${trip_id}/${day}/addJourney`,{
                method:"POST",
                headers:{
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${getState().auth.token}`
                },
                body: JSON.stringify({ trip_id, day, location_name,address,lat,lng,note,start_time})
            })

            const json = await res.json();

            if (res.status != 200) {
                return 
            }
            dispatch(addJourney(json.id, json.location_name, json.trip_id, json.day, json.address,json.lat, json.lng ,json.note, json.start_time))

        }catch(e){
            console.error(e)
        }
    }
}