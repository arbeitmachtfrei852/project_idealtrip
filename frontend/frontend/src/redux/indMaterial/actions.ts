import { ThunkDispatch, RootState } from '../../store'

export interface Individual {
    id: number,
    item: string,
    user_id: number,
    trip_id: number,
    checked: boolean
}

export function loadIndMaterials(Individual: Individual[]) {
    return {
        type: "LOAD_IND" as "LOAD_IND",
        Individual
    }
}

export function addIndMaterials(id: number, item: string, user_id: number, trip_id: number, checked: boolean) {
    return {
        type: "@@MATERIALS/ADD_IND" as "@@MATERIALS/ADD_IND",
        id,
        item,
        user_id,
        trip_id,
        checked
    }
}

export function removeIndMaterials(item_id:number|undefined) {
    return {
        type: "REMOVE" as "REMOVE",
        item_id
    }
}


export function checkedMaterials(item_id:number|undefined){
    return{
        type: "CHECKED" as "CHECKED",
        item_id
    }
}

export type IndMaterialsAction = ReturnType<typeof loadIndMaterials | typeof addIndMaterials | typeof removeIndMaterials| typeof checkedMaterials>


export function getIndMaterial(trip_id: any) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/individualMaterials/${trip_id}/getIndMaterial`, {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`
                }
            })
            const json = await res.json();
            dispatch(loadIndMaterials(json))
        }
        catch (e) {
            console.error(e)
        }
    }
}



export function postIndMaterials(trip_id: any, item: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/individualMaterials/${trip_id}/addIndMaterial`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${getState().auth.token}`,
                },
                body: JSON.stringify({ trip_id, item })
            })
            const json = await res.json();
            if (res.status != 200) {
                return
            }
            dispatch(addIndMaterials(json.id, json.item, json.user_id, json.trip_id, json.checked))
        } catch (e) {
            console.error(e)
        }
    }
}

// export function putMaterials(item_id: number | undefined){
//     return async(dispatch: ThunkDispatch, getState: ()=> RootState)=>{
//         try{
//             await fetch(`${process.env.REACT_APP_BACKEND_URL}/individualMaterials/${item_id}/checkedIndMaterial`,{
//                 method: "PUT",
//                 headers: {
//                     Authorization: `Bearer ${getState().auth.token}`,
//                 },
//             })
//             dispatch(checkedMaterials(item_id, checked))
//         } catch(e){
//             console.error(e)
//         }
//     }
// }

export function deleteMaterials(item_id: number | undefined) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            await fetch(
                `${process.env.REACT_APP_BACKEND_URL}/individualMaterials/${item_id}/deleteIndMaterial`,
                {
                    method: "PUT",
                    headers: {
                        Authorization: `Bearer ${getState().auth.token}`,
                    },
                });

            dispatch(removeIndMaterials(item_id))

        } catch (e) {
            console.error(e)
        }
    }
}


export function updateMaterials(item_id: number){
    return async (dispatch: ThunkDispatch, getState: ()=> RootState)=>{
        try{
            await fetch(
                `${process.env.REACT_APP_BACKEND_URL}/individualMaterials/${item_id}/checkedIndMaterial`,
                {
                    method: "PUT",
                    headers: {
                        Authorization: `Bearer ${getState().auth.token}`,
                    },
                })
                dispatch(checkedMaterials(item_id))
        }catch(e){
            console.error(e)
        }
    }
}