import { IndMaterialsAction, Individual } from './actions'
import { stat } from 'fs'


export interface IndMaterialsState {
    individual: Individual[];
    error: string | null;
}

const initialState: IndMaterialsState = {
    individual: [],
    error: null
}


export function IndMaterialReducer(state: IndMaterialsState = initialState, action: IndMaterialsAction) {
    switch (action.type) {
        case "LOAD_IND":
            return {
                ...state,
                individual: action.Individual.map(data => data)
            }
        case "@@MATERIALS/ADD_IND":
            return {
                ...state,
                individual: [
                    {
                        id: action.id,
                        item: action.item,
                        user_id: action.user_id,
                        trip_id: action.trip_id,
                        checked: action.checked
                    },
                    ...state.individual
                ]
            }
        case "REMOVE" :
      
            return {
                ...state,
                individual: state.individual.filter(individual =>
                    individual.id !== action.item_id)
            }

        case "CHECKED":
            return {
                ...state,
                
            }

        default:
            return state
    }
}