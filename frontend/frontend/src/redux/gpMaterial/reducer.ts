import { GroupMaterialActions, Group } from './actions'

export interface GroupMaterialsState {
    groups: Group[]
}

const initialState: GroupMaterialsState = {
    groups: [],
}


export function GroupMaterialReducer(state: GroupMaterialsState = initialState, action: GroupMaterialActions): GroupMaterialsState {
    switch (action.type) {
        case "GROUP":
            return {
                groups: action.groups.map(data => data)
            }
        case "ADD":
            return {
                ...state,
                groups:[
                    {   
                        id: action.id,
                        item: action.item,
                        assignee: action.assignee,
                        trip_id: action.trip_id,
                        checked: action.checked
                    },
                    ...state.groups
                ]
            }
        case "REMOVE":
            return{
                ...state,
                groups: state.groups.filter(group=>
                    group.id !== action.item_id)
            }

        default:
            return state;
    }
}