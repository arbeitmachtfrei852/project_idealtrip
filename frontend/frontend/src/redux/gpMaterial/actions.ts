import { ThunkDispatch, RootState } from "../../store"

export interface Group {
    id: number,
    item: string,
    assignee: string,
    trip_id: number,
    checked: boolean
}

export function loadGroupMaterials(groups: Group[]) {
    return {
        type: "GROUP" as "GROUP",
        groups
    }
}

export function addGroupMaterials(id: number, item: string, assignee: string, trip_id: number, checked: boolean) {
    return {
        type: "ADD" as "ADD",
        id,
        item,
        assignee,
        trip_id,
        checked
    }
}

export function delGroupMaterials(item_id:number|undefined){
    return{
        type:"REMOVE" as "REMOVE",
        item_id
    }
}



export type GroupMaterialActions = ReturnType<typeof loadGroupMaterials | typeof addGroupMaterials| typeof delGroupMaterials>


export function postGroupMaterials(trip_id: any, item: string, assignee: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/groupMaterials/${trip_id}/addGroupMaterial`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${getState().auth.token}`
                },
                body: JSON.stringify({ trip_id, item, assignee })
            })

            const json = await res.json();

            if (res.status != 200) {
                return 
            }
            dispatch(addGroupMaterials(json.id, json.item, json.assignee, json.trip_id, json.checked))

        } catch (e) {
            console.error(e)
        }
    }
}

export function getGroupMaterials(trip_id: string | null) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/groupMaterials/${trip_id}/getGroupMaterial`, {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`
                }
            })
            const json = await res.json();
            console.log(json)
            dispatch(loadGroupMaterials(json))
        } catch (e) {
            console.error(e)
        }
    }
}

export function deleteGroupMaterials(item_id:number |undefined){
    return async (dispatch: ThunkDispatch,getState:()=>RootState)=>{
        try{
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/groupMaterials/${item_id}/deleteGroupMaterial`,{
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${getState().auth.token}`
                },
            })
            dispatch(delGroupMaterials(item_id))
        }catch(e){
            console.error(e)
        }
    }
}