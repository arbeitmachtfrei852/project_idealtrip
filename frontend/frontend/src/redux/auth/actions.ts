import { ThunkDispatch, RootState } from "../../store"
import { push } from "connected-react-router"
import useReactRouter from 'use-react-router';
export interface User {
    id: number;
    email: string;
}

export function loginSuccess(token: string, user: User) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
        user
    }
}
export function loginFailed(error: string|null) {
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
        error
    }
}
export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT' as '@@auth/LOGOUT'
    }
}
export function registerSuccess(email: string, password: string, confirmedpassword: string) {
    return {
        type: "@@auth/REGISTER_SUCCESS" as "@@auth/REGISTER_SUCCESS",
        email,
        password,
        confirmedpassword
    }
}
export function registerFail(error: string|null) {
    return {
        type: "@@auth/REGISTER_FAILED" as "@@auth/REGISTER_FAILED",
        error
    }
}

export type AuthActions = ReturnType<typeof registerFail | typeof registerSuccess | typeof loginSuccess | typeof loginFailed | typeof logoutSuccess>

export function register(email: string, password: string, confirmedpassword: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/register`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    { email, password, confirmedpassword }
                )
            })
            const json = await res.json();
  
            if (res.status != 200) {
                return dispatch(registerFail(json.error));
            }

            dispatch(registerSuccess(json.user, json.password, json.confirmedpassword))
            // dispatch(push(`/login`))

        } catch (e) {
            console.error(e);
            dispatch(registerFail('Unknown Error'));
        }
    }
}



export function logout() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        localStorage.removeItem('token')
        dispatch(logoutSuccess())
    };
}

export function login(email: string, password: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {

            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    { email, password }
                )
            })

            const json = await res.json();


            if (res.status != 200) {
                return dispatch(loginFailed(json.error));
            }

            if (!json.token) {
                return dispatch(loginFailed('Network Error'));
            }

            const returnPath = (getState().router.location.state as any)?.from

            localStorage.setItem('token', json.token); // 每次登入成功之前都會放條token係storage
            dispatch(loginSuccess(json.token, json.user));
            dispatch(push(returnPath || `/home?email=${json.user.email}&user_id=${json.user.id}`))
        } catch (e) {
            console.error(e);
            dispatch(loginFailed('Unknown Error'));
        }
    }
}

export function loginGoogle(accessToken: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/loginGoogle`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    { accessToken }
                )
            })

            const json = await res.json();

            if (res.status != 200) {
                return dispatch(loginFailed(json.error));
            }

            if (!json.token) {
                return dispatch(loginFailed('Network Error'));
            }

            const returnPath = (getState().router.location.state as any)?.from

            localStorage.setItem('token', json.token); // 每次登入成功之前都會放條token係storage
            dispatch(loginSuccess(json.token, json.user));
            dispatch(push(returnPath || `/home?email=${json.user.email}&user_id=${json.user.id}`))
        } catch (e) {
            console.error(e);
            dispatch(loginFailed('Unknown Error'));
        }
    }
}
export function loginFacebook(accessToken: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/loginFacebook`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    { accessToken }
                )
            })

            const json = await res.json();


            if (res.status != 200) {
                return dispatch(loginFailed(json.error));
            }

            if (!json.token) {
                return dispatch(loginFailed('Network Error'));
            }

            const returnPath = (getState().router.location.state as any)?.from

            localStorage.setItem('token', json.token); // 每次登入成功之前都會放條token係storage
            dispatch(loginSuccess(json.token, json.user));
            dispatch(push(returnPath || `/home?email=${json.user.email}&user_id=${json.user.id}`))
        } catch (e) {
            console.error(e);
            dispatch(loginFailed('Unknown Error'));
        }
    }
}
export function restoreLogin() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const token = localStorage.getItem('token');
            if (token == null) {
                dispatch(logout());
                return;
            }

            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const json = await res.json();

            if (json.id) {
                dispatch(loginSuccess(token, json))
            } else {
                dispatch(logout());
            }
        } catch (e) {
            console.error(e)
            dispatch(logout());
        }
    }
}