import { TripActions, Trip, Email } from "./actions";

export interface TripState{
    trips: Trip[];
    error: string | null;
    email: Email[];
}

const initialState: TripState = {
    trips: [],
    error: null,
    email: []
};

export function tripReducer(state: TripState = initialState, action: TripActions): TripState {
    switch (action.type) {
        case '@@trip/LOADED_TRIP':
            return {
                ...state,
                trips: action.trips.map(trip=>trip)
            }
        case '@@trip/ADDED_TRIP':
            return {
                ...state,
                trips:[
                    {
                        id: action.id,
                        trip_name:action.trip_name,
                        start_date: action.start_date,
                        end_date: action.end_date,
                        owner_id: action.owner_id,
                        trip_id: action.trip_id,
                    },
                    ...state.trips,
                ],
            }
        case '@@trip/DELETE_TRIP':
            return {
                ...state,
                trips:state.trips.filter((trip=>trip.id!==action.trip_id))
            }
        case '@@trip/ADD_FRIEND_TRIP':
            return{
                ...state,
            }
        case '@@trip/ADD_TRIP_FAILED_MSG':
            return{
                ...state,
                error: action.error
            }
        case '@@trip/ADD_FRIEND_FAILED_MSG':
            return{
                ...state,
                error: action.error
            }    
        case '@@trip/LOADED_ADDED_FRIEND_EMAIL':
            return{
                ...state,
                email: action.email.map(email=>email)
            }    

        default:
            return state;
    }
}
