import { ThunkDispatch, RootState } from "../../store";
import { push } from "connected-react-router";

export interface Trip {
  id: number | undefined;
  trip_name: string;
  start_date: Date;
  end_date: Date;
  owner_id: number;
  trip_id: number;
}

export interface Email {
  email: string
}

export function loadTripData(trips: Trip[]) {
  return {
    type: "@@trip/LOADED_TRIP" as "@@trip/LOADED_TRIP",
    trips,
  };
}

export function loadAddedFriendEmailData(email: Email[]) {
  return {
    type: "@@trip/LOADED_ADDED_FRIEND_EMAIL" as "@@trip/LOADED_ADDED_FRIEND_EMAIL",
    email
  }
}

export function addTripData(id: number, trip_name: string, start_date: Date, end_date: Date, owner_id: number, trip_id: number, ) {
  return {
    type: "@@trip/ADDED_TRIP" as "@@trip/ADDED_TRIP",
    id,
    trip_name,
    start_date,
    end_date,
    owner_id,
    trip_id,
  };
}

export function deleteTripData(trip_id: number | undefined) {
  return {
    type: "@@trip/DELETE_TRIP" as "@@trip/DELETE_TRIP",
    trip_id
  }
}

export function addFriendToTripData() {
  return {
    type: "@@trip/ADD_FRIEND_TRIP" as "@@trip/ADD_FRIEND_TRIP",
  }
}

export function addTripFailed(error: string | null) {
  return {
    type: "@@trip/ADD_TRIP_FAILED_MSG" as "@@trip/ADD_TRIP_FAILED_MSG",
    error
  }
}

export function addFriendFailed(error: string | null) {
  return {
    type: "@@trip/ADD_FRIEND_FAILED_MSG" as "@@trip/ADD_FRIEND_FAILED_MSG",
    error
  }
}



export type TripActions = ReturnType<typeof addFriendFailed | typeof loadAddedFriendEmailData | typeof addTripFailed | typeof loadTripData | typeof addTripData | typeof deleteTripData | typeof addFriendToTripData>;


export function getTrip() {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/trip`, {
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      });

      const json = await res.json();
      dispatch(loadTripData(json));

    } catch (e) {
      console.error(e);
    }
  };
}

export function addTrip(tripName: string, startDate: string, endDate: string) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/addTrip`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: JSON.stringify({ tripName, startDate, endDate }),
      });

      const json = await res.json();

      if (res.status != 200) {
        return dispatch(addTripFailed(json.error))
      }

      dispatch(addTripData(json.id, json.tripName, json.startDate, json.endDate, json.owner_id, json.trip_id));

    } catch (e) {
      console.error(e);
    }
  };
}

export function delTripData(showActionSheetTripId: number | undefined) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/trip/${showActionSheetTripId}/deletetrip`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${getState().auth.token}`,
          },
        });

      dispatch(deleteTripData(showActionSheetTripId))

    } catch (e) {
      console.error(e);
    }
  }
}

export function addFriendToTrip(email: string, showModalTripId: number | undefined) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND_URL}/trip/${showModalTripId}/addFriend`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getState().auth.token}`,
          },
          body: JSON.stringify({ email }),
        }
      );

      const json = await res.json();

      if (res.status != 200) {
        dispatch(addFriendFailed(json.error))
      }

    } catch (e) {
      console.error(e);
    }
  }
}

export function loadAddedEmail(showActionSheetTripId: number | undefined) {
  return async (dispatch: ThunkDispatch, getState: () => RootState) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/trip/${showActionSheetTripId}/list`, {
        headers: {
          Authorization: `Bearer ${getState().auth.token}`,
        },
      });
      const json = await res.json();
      dispatch(loadAddedFriendEmailData(json))

    } catch (e) {
      console.error(e);
    }
  }
}