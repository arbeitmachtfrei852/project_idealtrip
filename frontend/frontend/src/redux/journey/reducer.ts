import { Trip, Journey } from "../journey/actions";
import { JourneyActions } from "./actions";

export interface JourneyState {
    exactlyTrip: Trip [];
    journey: Journey [];
}

const initialState: JourneyState = {
    exactlyTrip: [],
    journey: []
}

export function journeyReducer(state: JourneyState = initialState, action: JourneyActions): JourneyState{
    switch(action.type){
        case '@@journey/LOADED_EXACTLY_TRIP':
            return{
                ...state,
                exactlyTrip: action.trip.map(trip=>trip)
            }
        case '@@journey/LOADED_JOURNEY':
            return{
                ...state,
                journey: action.journey.map(journey=>journey)
            }
        case "@@journey/DELETE_JOURNEY":
            // console.log(state.journey.filter(journey=>journey.journey.filter(journey=>journey.id !== action.journey_id)))

            return{
                ...state,
                journey: state.journey.filter(journey=>
                    journey.journey.filter(journey =>journey.id !== action.journey_id))
           
                // journey: state.journey.filter(journey =>
                //     journey.journey[0].id !== action.journey_id)
            }

        default:
            return state;
    }
}

