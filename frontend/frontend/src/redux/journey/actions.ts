import { RootState, ThunkDispatch } from "../../store"

export interface Trip {
    trip_name: string;
    start_date: Date;
    end_date: Date;
}

export interface Journey {
    allDate: Date,
    journey: [{
        id: number;
        location_name: string;
        address: string;
        lat: number;
        lng: number;
        trip_id: number;
        note: string;
        start_time: string;
    }]
}

export function loadExactlyTripData(trip: Trip[]) {
    return {
        type: "@@journey/LOADED_EXACTLY_TRIP" as "@@journey/LOADED_EXACTLY_TRIP",
        trip
    }
}

export function loadJourneyData(journey: Journey[]) {
    return {
        type: "@@journey/LOADED_JOURNEY" as "@@journey/LOADED_JOURNEY",
        journey
    }
}

export function deleteJourneyData(journey_id: number | undefined){
    console.log(`action: ${journey_id}`)
    return{
        type: "@@journey/DELETE_JOURNEY" as "@@journey/DELETE_JOURNEY",
        journey_id
    }
}

export type JourneyActions = ReturnType<typeof loadExactlyTripData | typeof loadJourneyData| typeof deleteJourneyData>

export function getExactlyTrip(trip_id: any) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/journey/${trip_id}/trip`, {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`
                }
            });

            const json = await res.json();
   
            dispatch(loadExactlyTripData(json))

        } catch (e) {
            console.error(e);
        }
    }
}

export function getJourney(trip_id: any) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/journey/${trip_id}`, {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`
                }
            });

            const json = await res.json();
            dispatch(loadJourneyData(json))
        } catch (e) {
            console.error(e);
        }
    }
}

export function delJourney(journey_id:number|undefined){
    return async (dispatch: ThunkDispatch, getState: ()=> RootState)=>{
        try{
            await fetch(`${process.env.REACT_APP_BACKEND_URL}/journey/${journey_id}/deleteJourney`,
            {
                method: "PUT",
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`,
                },
            })
            dispatch(deleteJourneyData(journey_id))
        }catch(e){
            console.error(e)
        }
    }
}